<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::controller('api', 'APIv1');
Route::resource('user', 'UserController', array('only' => array('show', 'edit')));
Route::resource('project', 'ProjectController');
Route::resource('note', 'NoteController');

Route::get('/', array('as' => 'main', function() {
  if(Auth::check()) {
    return Redirect::to('dashboard');
  }
    return View::make('SignUp2');
}));

//todo cambiar ruta
Route::get('validate', function() {
    return View::make('registro');
});

Route::get('login', function() {
    return View::make('login');
});

Route::get('forgot-password', array('as' => 'forgot.password', 
    'before' => 'is.not.authenticated', 
    function() {
return View::make('recuperarContra');
}));

Route::get('reset-password', array('before'=>'email.validate',function() {
    return View::make('recuperarContra2');
}));

Route::get('test', function() {
    return View::make('test');
});

Route::get('dashboard', function() {
    return View::make('dashProyectos');
});

Route::get('proyecto/{id}', function($id) {
    return View::make('dashProyecto', array('project' => $id));
});

Route::get('grupos', function() {
    return View::make('dashGrupos');
});

Route::get('grupo/{id}', function($id) {
    return View::make('dashGrupo', array('group' => $id));
});

Route::get('mensajes', function() {
    return View::make('mensajes');
});

Route::get('angular', function() {
    return View::make('dash');
});

Route::get('grupo', function () {
});

Route::get('privacidad', function() {
  return View::make('static.AvisoPrivacidad');
});

Route::get('terminos', function() {
  return View::make('static.TerminosCondiciones');
});

Route::get('contacto', function() {
  return View::make('static.Contacto');
});

Route::get('faq', function() {
  return View::make('static.PreguntasFrecuentes');
});

Route::get('copyright', function() {
return View::make('static.Copyright');
});

Route::get('recuperaPass', function() {
return View::make('static.RecuperarPass');
});
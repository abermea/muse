<?php

class GroupPost extends \Eloquent {
    protected $table = 'group_post';

    // Add your validation rules here
    public static $rules = [
            // 'title' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function group() {
        return $this->belongsTo('Group', 'groups_id', 'id');
    }
    
    public function user() {
        return $this->belongsTo('User');
    }

    public function comments() {
        return $this->hasMany('GroupPostComment');
    }
    
    public function likes() {
        return $this->hasMany('GroupPostLike');
    }
}
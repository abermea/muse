<?php

class GroupPostComment extends \Eloquent {

    // Add your validation rules here
    public static $rules = [
            // 'title' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function post() {
        return $this->belongsTo('GroupPost', 'group_post_id');
    }
    
    public function user() {
        return $this->belongsTo('User');
    }
}

<?php

class Group extends \Eloquent {

    protected $table = 'groups';
    // Add your validation rules here
    public static $rules = [
            // 'title' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function posts() {
        return $this->hasMany('GroupPost','groups_id','id');
    }

    public function users() {
        return $this->belongsToMany('User', 'group_user', 'group_id', 'user_id');
    }

    public function admin() {
        return $this->belongsTo('User', 'user_id');
    }

}

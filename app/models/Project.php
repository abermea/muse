<?php

class Project extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $guarded = array('');
    protected $dates = ['deleted_at'];
    
    public function getStartDateAttribute() {
        return $this->attributes['start-date'];
    }

    public function setStartDateAttribute($value) {
        return $this->attributes['start-date'] = $value;
    }
    
    public function getEndDateAttribute() {
        return $this->attributes['end-date'];
    }

    public function setEndDateAttribute($value) {
        return $this->attributes['end-date'] = $value;
    }

    public function users() {
        return $this->belongsToMany('User', 'project_user', 'project_id', 'user_id');
    }
    
    public function admin() {
        return $this->belongsTo('User', 'user_id', 'id');
    }
    
    public function activity() {
        return $this->hasMany('Activity');
    }
    
    public function file() {
        return $this->hasMany('File');
    }
    
    public function post() {
        return $this->hasMany('Post');
    }

    public function note() {
        return $this->hasMany('Note');
    }
    
    public function appointments() {
        return $this->hasMany('Appointment');
    }

    public static $rules = array(
        'name' => 'required',
        'description' => 'sometimes|max:500',
        'start-date' => 'sometimes',
        'end-date' => 'sometimes'
    );
    
    

}

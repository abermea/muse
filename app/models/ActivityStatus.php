<?php

class ActivityStatus extends \Eloquent {
    protected $table = 'activity_status';
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function activities() {
        return $this->hasMany('Activity');
    }
    
    public function scopeName($query, $name) {
        return $query->whereName($name);
    }

    public static $rules = array(
    );
}
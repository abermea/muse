<?php

class UFile extends \Eloquent {
    protected $table = 'file';
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function author() {
        return $this->belongsTo('User', 'user_id', 'id');
    }
    
    public function project() {
        return $this->belongsTo('Project');
    }
    
    public function scopeParentProject($query, $project) {
        return $query->whereProjectId($project);
    }

    public static $rules = array(
        'project' => 'required|exists:project,id'
    );
}
<?php

class Activity extends \Eloquent {

    protected $table = 'activity';
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function status() {
        return $this->belongsTo('ActivityStatus', 'activity_status_id', 'id');
    }

    public function project() {
        return $this->belongsTo('Project', 'project_id', 'id');
    }

    public function manager() {
        return $this->belongsTo('User', 'manager', 'id');
    }

    public function responsible() {
        return $this->belongsTo('User', 'responsible', 'id');
    }

    public function scopeResponsibleUser($query, $user) {
        return $query->whereResponsible($user);
    }

    public function scopeParentProject($query, $project) {
        return $query->whereProjectId($project);
    }

    public static $rules = array(
        'name' => 'required',
        'description' => 'sometimes',
        'responsible' => 'required|exists:user,id',
        'priority' => 'required|numeric',
        'start_date' => 'required',
        'end_date' => 'required',
        'project' => 'required|exists:project,id',
        'status' => 'sometimes|exists:activity_status,id'
    );

}

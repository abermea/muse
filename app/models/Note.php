<?php

class Note extends \Eloquent {
    
    protected $table = 'note';

    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function user() {
        return $this->belongsTo('User');
    }

    public function project() {
        return $this->belongsTo('Project');
    }
    
    public function note_likes() {
        return $this->hasMany('NoteLike', 'id', 'note_like_id');
    }
    
    public function note_comments() {
        return $this->hasMany('NoteComment', 'id', 'note_comment_id');
    }

    public function scopeAuthor($query, $userId) {
        return $query->whereUserId($userId);
    }

    public function scopeParentProject($query, $projectId) {
        return $query->whereProjectId($projectId);
    }

    public static $rules = array(
        'project' => 'required|exists:project,id',
        'content' => 'required'
    );

}

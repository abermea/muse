<?php

class Appointment extends \Eloquent {

    protected $table = 'appointment';
    // Add your validation rules here
    public static $rules = [
            // 'title' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function project() {
        return $this->belongsTo('Project');
    }

    public function host() {
        return $this->belongsTo('User', 'host_id', 'id');
    }

    public function guests() {
        return $this->belongsToMany('User', 'appointment_user', 'appointment_id', 'user_id');
    }

    public function scopeProject($query, Project $project) {
        return $query->whereHas('Project', function($query) use($project) {
                    $query->where('project_id', '=', $project->id);
                });
    }

}

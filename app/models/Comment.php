<?php

class Comment extends \Eloquent {

    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function author() {
        return $this->belongsTo('User', 'id', 'user_id');
    }

    public function post() {
        return $this->belongsTo('Post');
    }

    public static $rules = array(
        'post' => 'required|exists:post,id',
        'content' => 'required'
    );

}

<?php

class NoteComment extends \Eloquent {

    // Add your validation rules here
    public static $rules = [
            // 'title' => 'required'
        'note_id' => 'required|exists:note,id',
        'content' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function note() {
        return $this->belongsTo('Note');
    }
    
    public function user() {
        return $this->belongsTo('User');
    }
}

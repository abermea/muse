<?php

class Like extends \Eloquent {
    protected $table = 'like';
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function post() {
        return $this->belongsTo('Post');
    }
    
    public function user() {
        return $this->belongsTo('User');
    }

    public static $rules = array(
        'post' => 'required|exists:post,id'
    );
}
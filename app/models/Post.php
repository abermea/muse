<?php

class Post extends \Eloquent {
    protected $table = 'post';
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function post_type() {
        return $this->belongsTo('PostType', 'post_type_id', 'id');
    }
    
    public function project() {
        return $this->belongsTo('Project');
    }
    
    public function comments() {
        return $this->hasMany('Comment');
    }
    
    public function scopeParentProject($query, $project) {
        return $query->whereProjectId($project);
    }

    public static $rules = array(
        'project' => 'required',
        'content' => 'required',
    );

}

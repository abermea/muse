<?php

class PostType extends \Eloquent {

    protected $table = 'post_type';
    protected $fillable = ['*'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function posts() {
        return $this->hasMany('Post');
    }

    public static $rules = array(
    );

}

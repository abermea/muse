<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait,
        SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'validation-code', 'remember_token', 
        'created_at', 'updated_at', 'deleted_at');
    protected $guarded = array('');
    protected $dates = ['deleted_at'];

    public function getFirstNameAttribute() {
        return $this->attributes['first-name'];
    }

    public function setFirstNameAttribute($value) {
        $this->attributes['first-name'] = $value;
    }

    public function getLastNameAttribute() {
        return $this->attributes['last-name'];
    }

    public function setLastNameAttribute($value) {
        $this->attributes['last-name'] = $value;
    }
    
    public function getValidationCodeAttribute() {
        return $this->attributes['validation-code'];
    }

    public function setValidationCodeAttribute($value) {
        $this->attributes['validation-code'] = $value;
    }
    
    public function projects() {
        return $this->belongsToMany('Project', 'project_user', 'user_id', 'project_id');
    }
    
    public function owned_proyects() {
        return $this->hasMany('Project');
    }
    
    public function owned_groups() {
        return $this->hasMany('Group');
    }
    
    public function owned_appointments() {
        return $this->hasMany('Appointment');
    }

    public function groups() {
        return $this->belongsToMany('Group', 'group_user', 'user_id','group_id');
    }
    
    public function appointments() {
        return $this->belongsToMany('Appointment');
    }
    
    public function likes() {
        return $this->hasMany('Like');
    }
    
    public function comments() {
        return $this->hasMany('Comment');
    }
    
    public function managed_activities() {
        return $this->hasMany('Activity', 'manager');
    }
    
    public function assigned_activities() {
        return $this->hasMany('Activity', 'responsible');
    }
    
    public function files() {
        return $this->hasMany('File');
    }
    
    public function notes() {
        return $this->hasMany('Note');
    }
    
    public function note_likes() {
        return $this->hasMany('NoteLike');
    }
    
    public function note_comments() {
        return $this->hasMany('NoteComment');
    }
    
    public function group_posts() {
        return $this->hasMany('GroupPost');
    }
    
    public function group_post_comments() {
        return $this->hasMany('GroupPostComment');
    }
    
    public function group_post_like() {
        return $this->hasMany('GroupPostLike');
    }

    public static $create_rules = array(
        'email' => 'required|email|unique:user,email',
        'validation-code' => 'required|unique:user,validation-code'
    );
    public static $registration_rules = array(
        'email' => 'required|exists:user,email',
        'validation-code' => 'required|exists:user,validation-code'
    );
    public static $complete_rules = array(
        'email' => 'required|email|exists:user,email',
        'alias' => 'required|unique:user|alpha_dash|min:6|max:30',
        'password' => 'required|min:6',
        'first-name' => 'sometimes|alpha',
        'last-name' => 'sometimes|alpha',
        'message' => 'sometimes|max:150'
    );
    public static $alias_rules = array(
        'alias' => 'required|exists:user,alias|alpha_dash|min:6|max:30'
    );
    public static $edit_rules = array(
        'email' => 'required|email|exists:user,email',
        'alias' => 'required|unique:user|alpha_dash|min:6|max:30',
        'old-password' => 'required|min:6',
        'new-password' => 'sometimes|min:6',
        'first-name' => 'sometimes|alpha',
        'last-name' => 'sometimes|alpha',
        'message' => 'sometimes|max:150'
    );
    public static $reset_rules = array(
        'email' => 'required|email|exists:user,email',
        'validation-code' => 'required|exists:user,validation-code',
        'password' => 'required|min:6',
    );

}

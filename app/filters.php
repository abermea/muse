<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

Route::filter('has.email', function() {
    if (!Input::has('email')) {
        return Response::make('', 400, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('has.email.and.pass', function() {
    if (!Input::has('alias') or !Input::has('password') or !Input::has('remember-me')) {
        return Response::make('', 400, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('has.alias', function() {
    if (!Input::has('alias')) {
        return Response::make('', 400, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('alias.exists', function() {
    $alias = Input::all();
    $validator = Validator::make($alias, User::$alias_rules);
    if ($validator->fails()) {
        return Response::make('', 404, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('email.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, User::$registration_rules);
    if ($validator->fails()) {
        return Response::make('', 401, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('user.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, User::$complete_rules);
    if ($validator->fails()) {
        Log::info($new);
        return Response::make('', 400, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('reset.password.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, User::$reset_rules);
    if ($validator->fails()) {
        return Response::make('', 401, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('is.authenticated', function() {
    if (!Auth::check()) {
        Log::info('[is.authenticated] Auth Failed ');
        return Response::make('', 401, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('is.not.authenticated', function() {
    if (Auth::check()) {
        Log::info('[is.not.authenticated] Auth Succeded ');
        return Response::make('', 401, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('user.edit', function() {
    $new = Input::all();
    $validator = Validator::make($new, User::$edit_rules);
    if ($validator->fails()) {
        foreach ($validator->messages() as $message) {
            Log::info('[user.edit] Messages: ' . $message);
        }
        return Response::make('', 400, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('is.user.pass', function() {
    if (Auth::user()->password != Hash::make(Input::get('old-password'))) {
        Log::info('[is.user.pass] Submitted password: ' . Input::get('old-password'));
        return Response::make('', 401, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('project.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, Project::$rules);
    if ($validator->fails()) {
        foreach ($validator->messages()->all() as $message) {
            Log::info('[project.validate] Messages: ' . $message);
        }
        return Response::make('', 400);
    }
});

Route::filter('is.project.admin', function() {
    $project = Project::find(Input::get('id'));
    if (!($project->admin->id == Auth::user()->id)) {
        return Response::make('', 401, array('Content-Type' => 'text/plain'));
    }
});

Route::filter('note.validate', function() {
    $new = Input::all();
    Log::info($new);
    $validator = Validator::make($new, Note::$rules);
    if ($validator->fails()) {
        foreach ($validator->messages()->all() as $message) {
            Log::info('[note.validate] Messages: ' . $message);
        }
        return Response::make('', 400);
    }
});

Route::filter('file.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, UFile::$rules);
    if ($validator->fails() || !Input::hasFile('files')) {
        foreach ($validator->messages()->all() as $message) {
            Log::info('[file.validate] Messages: ' . $message);
        }
        if(!Input::hasFile('files')) {
            Log::info('[file.validate] Message: No files on Request');
        }
        return Response::make('', 400);
    }
});

Route::filter('comment.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, Comment::$rules);
    if ($validator->fails()) {
        foreach ($validator->messages()->all() as $message) {
            Log::info('[comment.validate] Messages: ' . $message);
        }
        return Response::make('', 400);
    }
});

Route::filter('post.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, Post::$rules);
    if ($validator->fails()) {
        foreach ($validator->messages()->all() as $message) {
            Log::info('[post.validate] Messages: ' . $message);
        }
        return Response::make('', 400);
    }
});

Route::filter('activity.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, Activity::$rules);
    if ($validator->fails()) {
        foreach ($validator->messages()->all() as $message) {
            Log::info('[activity.validate] Messages: ' . $message);
        }
        return Response::make('', 400);
    }
});

Route::filter('like.validate', function() {
    $new = Input::all();
    $validator = Validator::make($new, Like::$rules);
    if ($validator->fails()) {
        foreach ($validator->messages()->all() as $message) {
            Log::info('[like.validate] Messages: ' . $message);
        }
        return Response::make('', 400);
    }
});

Route::filter('is.admin.or.comment.owner', function() {
    $comment = Comment::findOrFail(Input::get('comment'));
    $values = [ $comment->user_id, $comment->post->project->admin->id ];
    if (!in_array(Auth::user()->id, $values)) {
        Log::info('[is.admin.or.comment.owner] Message: El usuario no es dueño ni admin');
        return Response::make('', 400);
    }
});

Route::filter('is.admin.or.post.owner', function() {
    $post = Post::findOrFail(Input::get('post'));
    $values = [ $post->user_id, $post->post->project->admin->id ];
    if (!in_array(Auth::user()->id, $values)) {
        Log::info('[is.admin.or.comment.owner] Message: El usuario no es dueño ni admin');
        return Response::make('', 400);
    }
});

Route::filter('is.admin.or.post.owner', function() {
    $post = Post::findOrFail(Input::get('post'));
    $values = [ $post->author->id, $post->post->project->admin->id ];
    if (!in_array(Auth::user()->id, $values)) {
        Log::info('[is.admin.or.comment.owner] Message: El usuario no es dueño ni admin');
        return Response::make('', 400);
    }
});

Route::filter('is.admin.or.file.owner', function() {
    $file = UFile::findOrFail(Input::get('file'));
    Log::info($file->project->admin);
    $values = [ $file->author->id, $file->project->admin->id ];
    if (!in_array(Auth::user()->id, $values)) {
        Log::info('[is.admin.or.file.owner] Message: El usuario no es dueño ni admin');
        return Response::make('', 400);
    }
});

Route::filter('is.admin.or.post.owner', function() {
    $post = Post::findOrFail(Input::get('post'));
    $values = [ $post->user_id, $post->project->admin->id ];
    if (!in_array(Auth::user()->id, $values)) {
        Log::info('[is.admin.or.post.owner] Message: El usuario no es dueño ni admin');
        return Response::make('', 400);
    }
});

Route::filter('is.manager.or.responsible', function() {
    $activity = Activity::findOrFail(Input::get('activity'));
    $values = [ $activity->manager->id, $activity->responsible->id ];
    if (!in_array(Auth::user()->id, $values)) {
        Log::info('[is.manager.or.responsible] Message: El usuario no es dueño ni admin');
        return Response::make('', 400);
    }
});

Route::filter('note.post', function($route, $request, $response) {
    $content = json_decode($response->getContent(), true);
    $post = new Post();
    $post->project_id = $content['project']['id'];
    $post->data = json_encode(['user' => $content['user']['id'], 'note' => $content['note']['id']]);
    $post->post_type_id = 3;
    $post->save();
    return Response::json($content['note']);
});

Route::filter('file.post', function($route, $request, $response) {
    $content = json_decode($response->getContent(), true);
    $post = new Post();
    $post->project_id = $content['project']['id'];
    $post->data = json_encode(['user' => $content['user']['id'], 'file' => $content['file']['id']]);
    $post->post_type_id = 1;
    $post->save();
    return Response::json($content['files']);
});

Route::filter('task.post', function($route, $request, $response) {
    $content = json_decode($response->getContent(), true);
    $post = new Post();
    $post->project_id = $content['project'];
    $post->data = json_encode(['manager' => $content['manager'],'responsible' => $content['responsible'], 'task' => $content['task']['id']]);
    $post->post_type_id = 4;
    $post->save();
    return Response::json($content['task']);
});
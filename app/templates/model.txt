<?php

class $NAME$ extends \Eloquent {
    protected $fillable = [];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
    );
}
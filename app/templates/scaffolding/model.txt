<?php

class $NAME$ extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];
        protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
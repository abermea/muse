<?php

class APIv1 extends \BaseController {

    public function __construct() {
        $this->beforeFilter('has.email', array('only' => array('postRegister', 'postPasswordRetrieval')));
        $this->beforeFilter('has.email.and.pass', array('only' => 'postAuthenticate'));
        $this->beforeFilter('email.validate', array('only' => array('postCheckMailValidationCode')));
        $this->beforeFilter('user.validate', array('only' => 'postCompleteRegistration'));
        $this->beforeFilter('is.authenticated', array('except' => array('postRegister', 'postSimpleRegister', 'postPasswordRetrieval', 'postResetPassword', 'postCheckMailValidationCode', 'postCompleteRegistration', 'postSimpleAuth', 'postAuthenticate', 'postResendEmail')));
        $this->beforeFilter('has.alias', array('only' => 'postUser'));
        $this->beforeFilter('alias.exists', array('only' => 'postUser'));
        $this->beforeFilter('user.edit', array('only' => 'patchUser'));
        $this->beforeFilter('is.user.pass', array('only' => 'patchUser'));
        $this->beforeFilter('reset.password.validate', array('only' => 'postResetPassword'));

        $this->beforeFilter('project.validate', array('only' => array('postProjectCreate', 'postProjectUpdate')));
        $this->beforeFilter('is.project.admin', array('only' => array('postProjectUpdate', 'postProjectDelete')));

        $this->beforeFilter('like.validate', array('only' => array('postLikeCreate')));
        $this->beforeFilter('comment.validate', array('only' => array('postCommentCreate', 'postCommentUpdate')));
        $this->beforeFilter('activity.validate', array('only' => array('postActivityCreate')));
        $this->beforeFilter('file.validate', array('only' => array('postFileCreate')));
        $this->beforeFilter('note.validate', array('only' => array('postNoteCreate', 'postNoteUpdate')));
        $this->beforeFilter('post.validate', array('only' => array('postPostCreate')));

        $this->beforeFilter('like.validate', array('only' => array('postLikeCreate')));
        $this->beforeFilter('is.admin.or.comment.owner', array('only' => array('postCommentDelete')));
        $this->beforeFilter('is.manager.or.responsible', array('only' => array('postActivityUpdate', 'postActivityUpdateStatus')));
        $this->beforeFilter('is.admin.or.file.owner', array('only' => array('postFileDelete')));
        $this->beforeFilter('is.admin.or.note.owner', array('only' => array('postNoteDelete', 'postNoteUpdate')));
        $this->beforeFilter('is.admin.or.post.owner', array('only' => array('postPostDelete')));

        $this->afterFilter('file.post', array('only' => array('postFileCreate')));
        $this->afterFilter('note.post', array('only' => array('postNoteCreate')));
        $this->afterFilter('task.post', array('only' => array('postActivityCreate')));
    }

    public function postRegister() {
        $new = array(
            'email' => Input::get('email'),
            'validation-code' => bin2hex(openssl_random_pseudo_bytes(30)),
            'validated' => false
        );

        $validator = Validator::make($new, User::$create_rules);

        if ($validator->messages()->get('email')) {
            return Response::make('', 409, array('Content-Type' => 'text/plain'));
        }

        while ($validator->messages()->get('validation-code')) {
            $new['validation-code'] = bin2hex(openssl_random_pseudo_bytes(30));
        }

        Mail::send('emails.auth.correoBienvenida', array('email' => $new['email'], 'validationCode' => $new['validation-code']), function ($message) {
            $message->from('muse@muse.com', 'Muse')->to(Input::get('email'), 'Usuario')->subject('Bienvenido a Muse');
        });

        User::create($new);
        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function postResendEmail() {
        $new = User::where('email', '=', Input::get('email'))->first();
        Mail::send('emails.auth.correoBienvenida', array('email' => $new['email'], 'validationCode' => $new['validation-code']), function ($message) {
            $message->from('muse@muse.com', 'Muse')
                    ->to(Input::get('email'), 'Usuario')
                    ->subject('Bienvenido a Muse');
        });
        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function postPasswordRetrieval() {
        set_time_limit(180);
        $new = User::where('email', '=', Input::get('email'))->first();
        Log::info($new);
        $new->password = '';
        $new->validated = false;
        $validator = Validator::make($new->toArray(), User::$registration_rules);

        if ($validator->messages()->get('email')) {
            return Response::make('', 404, array('Content-Type' => 'text/plain'));
        }

        while ($validator->messages()->get('validation-code')) {
            $new['validation-code'] = bin2hex(openssl_random_pseudo_bytes(30));
        }

        Mail::send('emails.auth.correoRecuperarContra', array('email' => $new['email'], 'validationCode' => $new['validation-code']), function ($message) {
            $message->from('muse@muse.com', 'Muse')
                    ->to(Input::get('email'), 'Usuario')
                    ->subject('Recupera tu contraseña de Muse');
        });
        $new->save();
        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function postResetPassword() {
        $new = User::where('email', '=', Input::get('email'))->first();
        $new->password = Hash::make(Input::get('password'));
        $new->validated = true;
        $new->save();
        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function postCheckMailValidationCode() {
        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function postCompleteRegistration() {
        // TODO agregar carga del avatar
        $new = User::where('email', '=', Input::get('email'))->first();
        $new->alias = Input::get('alias');
        $new->first_name = Input::get('first-name');
        $new->last_name = Input::get('last-name');
        $new->message = Input::get('message');
        $new->password = Hash::make(Input::get('password'));
        $new->validated = true;
        $new->save();

        Mail::send('emails.auth.correoCuentaCreada', array(), function ($message) use($new) {
                    $message->from('muse@muse.com', 'Muse')->to($new->email, 'Usuario')->subject('Gracias por unirte a Muse');
                });

        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function postSimpleRegister() {
        $new = new User();
        $new->email = Input::get('alias');
        $new->password = Hash::make(Input::get('password'));
        $new->validated = true;

        $new['validation-code'] = bin2hex(openssl_random_pseudo_bytes(30));
        $new->save();

        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function postAuthenticate() {
        $email = Input::get('alias');
        $password = Input::get('password');
        $remember_me = Input::get('remember-me');
        if (Auth::attempt(array('email' => $email, 'password' => $password), $remember_me) or Auth::attempt(array('alias' => $email, 'password' => $password), $remember_me)) {
            return Response::make('', 200, array('Content-Type' => 'text/plain'));
        }
        return Response::make('', 401, array('Content-Type' => 'text/plain'));
    }

    public function postSimpleAuth() {
        $email = Input::get('alias');
        $password = Input::get('password');
        if (Auth::attempt(array('email' => $email, 'password' => $password), true) or Auth::attempt(array('alias' => $email, 'password' => $password), true)) {
            return Response::make('', 200, array('Content-Type' => 'text/plain'));
        }
        return Response::make('', 401, array('Content-Type' => 'text/plain'));
    }

    public function postLogout() {
        Auth::logout();
        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function getUser() {
        $user = User::find(Input::get('alias'));

        return Response::make($user);
    }

    public function postUser() {
        $alias = Input::get('alias');
        $user = null;
        if (is_numeric($alias)) {
            $user = User::find($alias);
        } else {
            $user = User::where('alias', '=', $alias)->first();
        }
        Log::info('[api/user (POST)] Usuario: ' . $user);
        return Response::make($user);
    }

    public function postUserUpdate() {
        $user = Auth::user();
        Log::info('[api/user (PATCH)] Old Data: ' . $user);

        $user->alias = Input::get('alias');
        $user->first_name = Input::get('first-name');
        $user->last_name = Input::get('last-name');
        $user->message = Input::get('message');
        $user->email = Input::get('email');
        if (Input::has('new-password')) {
            $user->password = Hash::make(Input::get('new-password'));
        }
        $user->save();
        Log::info('[api/user (PATCH)] New Data: ' . $user);
        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function postUserDelete() {
        User::destroy(Auth::user()->id);
        Auth::logout();
        return Response::make('', 200, array('Content-Type' => 'text/plain'));
    }

    public function getProjects() {
        $data = User::find(Auth::user()->id)->projects;
        return Response::json($data);
    }

    public function postProjectCreate() {
        $start = DateTime::createFromFormat('d/m/Y', Input::get('start-date'));
        $end = DateTime::createFromFormat('d/m/Y', Input::get('end-date'));

        $new = array(
            'name' => Input::get('name'),
            'description' => Input::get('description'),
            'start-date' => !Input::has('start-date') ? null : $start->format('Y-m-d H:i:s'),
            'end-date' => !Input::has('end-date') ? null : $end->format('Y-m-d H:i:s'),
            'user_id' => Auth::user()->id
        );
        $project = Project::firstOrCreate($new);
        $user = User::find(Auth::user()->id);
        $user->projects()->save($project);
        return Response::make($project);
    }

    public function postProjectUpdate() {
        $project = Project::find(Input::get('id'));
        $project->name = Input::get('name');
        $project->description = Input::get('description');
        $project->start_date = Input::has('start-date') ? Input::get('start-date') : null;
        $project->end_date = Input::has('end-date') ? Input::get('end-date') : null;
        $project->save();
        return Response::make($project);
    }

    public function postProjectDelete() {
        $project = Project::findOrFail(Input::get('id'));
        $project->delete();
        return Response::make();
    }

    public function postInvite() {
        $emails = Input::get('emails');
        $project = Project::find(Input::get('project'));
        foreach ($emails as $email) {
            $new = array(
                'email' => $email,
                'validated' => true,
                'validation-code' => bin2hex(openssl_random_pseudo_bytes(30))
            );

            $validator = Validator::make($new, User::$create_rules);
            while ($validator->messages()->get('validation-code')) {
                $new['validation-code'] = bin2hex(openssl_random_pseudo_bytes(30));
            }

            if($validator->messages()->get('email')) {
                $user = User::where('email','=',$new['email']);
                // TODO correo bienvenida
                Mail::send('emails.auth.correoInvitacionUsuario', array(), function ($message) use($user) {
                    $message->from('muse@muse.com', 'Muse')->to($user->email, 'Usuario')->subject('Bienvenido a Muse');
                });
            } else {
                $user = User::firstOrCreate($new);

            Mail::send('emails.auth.correoBienvenida', array('email' => $user['email'], 'validationCode' => $user['validation-code']), function ($message) use($user) {
                $message->from('muse@muse.com', 'Muse')->to($user->email, 'Usuario')->subject('Bienvenido a Muse');
            });
            }

            

            $user->projects()->save($project);
            $user->save();
        }
        return Response::make('');
    }

    public function getUsersByProject() {
        $users = Project::findOrFail(Input::get('project'))->users;
        return Response::json($users);
    }

    public function getUsersByGroup() {
        $users = Group::findOrFail(Input::get('group'))->users;
        return Response::json($users);
    }

    public function getCurrentUser() {
        $user = User::findOrFail(Auth::user()->id);
        return Response::make($user);
    }

    public function getProject() {
        $project = Project::findOrFail(Input::get('project'));
        return Response::make($project);
    }

    public function postFileCreate() {
        $data = Input::file('files');
        $project = Project::findOrFail(Input::get('project'));
        $user = User::findOrFail(Auth::user()->id);
        $path = public_path() . '/files/';

        $new_name = bin2hex(openssl_random_pseudo_bytes(30));
        $data->move($path, $new_name);
        $file = new UFile();
        $file->original_filename = $data->getClientOriginalName();
        $file->file_uri = $new_name;
        $file->author()->associate($user);
        $file->project()->associate($project);
        $file->save();

        $response = array(
            'files' => compact($path . $new_name),
            'user' => $user,
            'project' => $project,
            'file' => $file
        );

        return Response::json($response, 200);
    }

    public function postFileDelete() {
        $file = UFile::find(Input::get('file'));
        $file->delete();
        return Response::make('');
    }

    public function getFilesProject() {
        $files = UFile::parentProject(Input::get('project'))->get();
        return Response::json($files);
    }

    public function getFileData() {
        $file = UFile::find(Input::get('file'));
        return Response::json($file);
    }

    public function getFile() {
        $file = UFile::find(Input::get('file'));
        $path = public_path() . '/files/' . $file->file_uri;
        return Response::download($path, $file->original_filename);
    }

    public function postCommentCreate() {
        $comment = new Comment();
        $comment->content = Inupt::get('content');
        $comment->author()->associate(User::findOrFail(Auth::user()->id));
        $comment->post()->associate(Post::findOrFail(Input::get('post')));
        $comment->save();
        return Response::make($comment);
    }

    public function postCommentUpdate() {
        $comment = Comment::findOrFail(Input::get('comment'));
        $comment->content = Input::get('content');
        $comment->update();
        return Response::make('');
    }

    public function postCommentDelete() {
        $comment = Comment::find(Input::get('comment'));
        $comment->delete();
        return Response::make('');
    }

    public function postLikeCreate() {
        $like = new Like();
        $like->post_id = Input::get('post');
        $like->user_id = Auth::user()->id;
        $like->save();
        return Response::make($like);
    }

    public function postLikeDelete() {
        $like = new Like();
        $like->user()->associate(User::findOrFail(Auth::user()->id));
        $like->post()->associate(Post::findOrFail(Input::get('post')));
        $result = Like::firstOrNew($like);
        $result->delete();
        return Response::make($result);
    }

    public function getActivityStatuses() {
        $statuses = ActivityStatus::all();
        return Response::make($statuses);
    }

    public function getActivity() {
        $activity = Activity::findOrFail(Input::get('activity'));
        return Response::make($activity);
    }

    public function postActivityCreate() {
        $activity = new Activity();

        $start = DateTime::createFromFormat('d/m/Y h:i A', Input::get('start_date') . ' ' . Input::get('start_time'));
        $end = DateTime::createFromFormat('d/m/Y h:i A', Input::get('end_date') . ' ' . Input::get('end_time'));
        $activity->name = Input::get('name');
        $activity->description = Input::get('description');
        $activity->begin_date = $start->format('Y-m-d H:i:s');
        $activity->end_date = $end->format('Y-m-d H:i:s');
        $activity->priority = Input::get('priority');
        $activity->manager = Auth::user()->id;
        $activity->responsible = Input::get('responsible');
        $activity->project_id = Input::get('project');
        $activity->activity_status_id = 1;

        $activity->save();
        return Response::json(['task' => $activity, 'manager' => Auth::user()->id, 'responsible' => Input::get('responsible'), 'project' => Input::get('project')]);
    }

    public function postActivityUpdate() {
        $activity = Activity::findOrFail(Input::get('activity'));
        $activity->name = Input::get('name');
        $activity->description = Input::get('description');
        $activity->attributes['responsible'] = Input::get('responsible');
        $activity->priority = Input::get('priority');
        $activity->begin_date = Input::has('begin_date') ? Input::get('begin_date') : null;
        $activity->end_date = Input::has('end_date') ? Input::get('end_date') : null;
        $activity->save();
        return Response::make($activity);
    }

    public function postActivityUpdateStatus() {
        $activity = Activity::findOrFail(Input::get('activity'));
        $activity->activity_status_id = Input::get('status');
        $activity->save();
        return Response::json($activity);
    }

    public function getActivitiesUser() {
        $activities = Activity::responsibleUser(Auth::user()->id)->get();
        return Response::json($activities);
    }

    public function getActivitiesProject() {
        $activities = Activity::parentProject(Input::get('project'))->get();
        return Response::json($activities);
    }

    public function getActivitiesUserProject() {
        $activities = Activity::responsibleUser(Auth::user()->id)->parentProject(Input::get('project'))->get();
        Log::info($activities);
        return Response::json($activities);
    }

    public function getActivitiesUserProjectCalendar() {
        $activities = Activity::responsibleUser(Auth::user()->id)->parentProject(Input::get('project'))->get();
        $response = array();
        foreach($activities as $activity){
            array_push($response, array(
                    'name' => $activity->name,
                    'description' => $activity->description,
                    'start' => $activity->begin_date,
                    'end' => $activity->end_date
                ));
        }
        return Response::json($response);
    }

    public function getProjectPost() {
        $posts = Post::parentProject(Input::get('project'))->orderBy('created_at','DESC')->get();
        return Response::json($posts);
    }

    public function postPostCreate() {
        $data = [
            'uid' => Auth::user()->id,
            'content' => Input::get('content')
        ];

        $post = new Post();
        $post->post_type()->associate(PostType::findOrFail(5));
        $post->project()->associate(Project::findOrFail(Input::get('project')));
        $post->data = json_encode($data);
        $post->save();
        return Response::make($post);
    }

    public function postPostDelete() {
        $post = $post::findOrFail(Input::get('post'));
        $post->delete();
        return Response::make();
    }

    public function getPostType() {
        $type = PostType::find(Input::get('type'));
        return Response::json($type);
    }

    public function getNote() {
        $note = Note::findOrFail(Input::get('note'));
        return Response::make($note);
    }

    public function getNotes() {
        $notes = Note::parentProject(Input::get('project'))->get();
        Log::info(Input::get('project'));
        Log::info($notes);
        return Response::json($notes);
    }

    public function postNoteCreate() {
        $author = User::findOrFail(Auth::user()->id);
        $project = Project::findOrFail(Input::get('project'));
        $note = new Note();
        $note->name = Input::get('name');
        $note->content = Input::get('content');
        $note->user()->associate($author);
        $note->project()->associate($project);
        $note->save();
        return Response::make(['project' => $project, 'note' => $note, 'user' => $author]);
    }

    public function postNoteUpdate() {
        $note = Note::findOrFail(Input::get('note'));
        $note->name = Input::get('name');
        $note->content = Input::get('content');
        $note->save();
        return Response::make($note);
    }

    public function postNoteDelete() {
        $note = Note::findOrFail(Input::get('note'));
        $note->delete();
        return Response::make($note);
    }

    public function postNoteLikeCreate() {
        $like = [
            'user_id' => Auth::user()->id,
            'note_id' => Input::get('note')
        ];

        $result = NoteLike::firstOrCreate($like);
        if ($result->trashed()) {
            $result->restore();
        }
        return Response::make($result);
    }

    public function postNoteLikeDelete() {
        $like = [
            'user_id' => Auth::user()->id,
            'note_id' => Input::get('note')
        ];

        $result = NoteLike::firstOrCreate($like);
        $result->delete();
        return Response::make($result);
    }

    public function postNoteCommentCreate() {
        $comment = [
            'note_id' => Input::get('note'),
            'user_id' => Auth::user()->id,
            'content' => Inupt::get('content')
        ];

        $result = NoteComment::create($comment);
        return Response::make($result);
    }

    public function postNoteCommentUpdate() {
        $comment = NoteComment::findOrFail(Input::get('comment'));
        $comment->content = Input::get('content');
        $comment->update();
        return Response::make('');
    }

    public function postNoteCommentDelete() {
        $comment = NoteComment::find(Input::get('comment'));
        $comment->delete();
        return Response::make('');
    }

    public function getGroup() {
        $group = Group::find(Input::get('group'));
        return Response::json($group);
    }

    public function getGroups() {
        $groups = User::find(Auth::user()->id)->groups;
        return Response::json($groups);
    }

    // TODO cargar imagen de grupo en create y update
    public function postGroupCreate() {
        $user = User::find(Auth::user()->id);
        $group = new Group();
        $group->name = Input::get('name');
        $group->description = Input::get('description');
        $group->picture_uri = '';
        $group->admin()->associate($user);
        $group->save();
        $user->groups()->save($group);
        return Response::json($group);
    }

    public function postGroupUpdate() {
        $group = Group::find(Input::get('group'));
        $group->name = Input::get('name');
        $group->description = Input::get('description');
        $group->picture_uri = '';
        $group->save();
        return Response::json($group);
    }

    public function postGroupDelete() {
        $group = Group::find(Input::get('group'));
        $group->delete();
        return Response::json($group);
    }

    public function postGroupUserAdd() {
        $group = Group::find(Input::get('group'));
        $user = User::find(Input::get('user'));
        $group->users()->attach($user);
        return Response::make();
    }

    public function getAppointment() {
        $appointment = Appointment::find(Input::get('appointment'));
        return Response::json($appointment);
    }

    public function getAppointmentsProject() {
        $project = Project::find(Input::get('project'));
        $appointments = Appointment::project($project)->get();
        return Response::json($appointments);
    }

    public function postAppointmentCreate() {
        $appointment = new Appointment();
        $project = Project::find(Input::get('project'));
        $start = DateTime::createFromFormat('d/m/Y h:i A', Input::get('start_date') . ' ' . Input::get('start_time'));
        $end = DateTime::createFromFormat('d/m/Y h:i A', Input::get('end_date') . ' ' . Input::get('end_time'));
        $appointment->name = Input::get('name');
        $appointment->description = Input::get('description');
        $appointment->start_date = $start->format('Y-m-d H:i:s');
        $appointment->end_date = $end->format('Y-m-d H:i:s');
        $appointment->location = Input::get('location');
        $appointment->project()->associate($project);
        $appointment->host()->associate(User::find(Auth::user()->id));
        $appointment->save();
        foreach ($project->users as $user) {
            $appointment->guests()->attach($user);
        }
        $appointment->save();
        return Response::json($appointment);
    }

    public function postAppointmentUpdate() {
        $appointment = Appointment::find(Input::get('appointment'));
        $start = DateTime::createFromFormat('d/m/Y h:i A', Input::get('start_date') . ' ' . Input::get('start_time'));
        $end = DateTime::createFromFormat('d/m/Y h:i A', Input::get('end_date') . ' ' . Input::get('end_time'));
        $appointment->name = Input::get('name');
        $appointment->description = Input::get('description');
        $appointment->start_date = $start->format('Y-m-d H:i:s');
        $appointment->end_date = $end->format('Y-m-d H:i:s');
        $appointment->location = Input::get('location');
        $appointment->save();
        return Response::json($appointment);
    }

    public function postAppointmentDelete() {
        $appointment = Appointment::find(Input::get('appointment'));
        $appointment->delete();
        return Response::json($appointment);
    }

    public function getGroupPosts() {
        $group = Group::findOrFail(Input::get('group'));
        $posts = $group->posts()->orderBy('created_at','DESC')->get();
        return Response::json($posts);
    }

    // TODO completar create de GroupPost
    public function postGroupPostCreate() {
        $post = new GroupPost();
        $post->data = Input::get('data');
        $post->user()->associate(User::find(Auth::user()->id));
        $post->group()->associate(Group::find(Input::get('group')));
        $post->save();
        return Response::json($post);
    }

    public function postGroupPostDelete() {
        $post = GroupPost::find(Input::get('group_post'));
        $post->delete();
        return Response::json($post);
    }

    public function getGroupPostComments() {
        $comments = GroupPost::find(Input::get('group_post'))->comments;
        return Response::json($comments);
    }

    // TODO completar create de GroupPostComment
    public function postGroupPostCommentCreate() {
        $comment = new GroupPostComment();
        $comment->content = Input::get('content');
        $comment->post()->associate(GroupPost::find(Input::get('group_post')));
        $comment->user()->associate(User::find(Auth::user()->id));
        return Response::json($comment);
    }

    public function postGroupPostCommentDelete() {
        $comment = GroupPostComment::find(Input::get('comment'));
        $comment->delete();
        return Response::json();
    }

    public function postGroupPostLike() {
        $like = new GroupPostLike();
        $like->post()->associate(GroupPost::find(Input::get('group_post')));
        $like->user()->associate(User::find(Auth::user()->id));
        $like->save();
        return Response::json();
    }

    public function postGroupPostUnlike() {
        $like = new GroupPostLike();
        $like->post()->associate(GroupPost::find(Input::get('group_post')));
        $like->user()->associate(User::find(Auth::user()->id));
        $like->delete();
        return Response::json();
    }

}

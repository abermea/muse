<?php

class UserController extends \BaseController {

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $user = API::post('api/user', array('alias' => $id));
        return View::make('usuario', array('user' => $user));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        // TODO arreglar pendejada para que no jale si le pones un alias que no seas tu
        $user = API::post('api/user', array('alias' => Auth::user()->id));
        return View::make('editarUsuario', array('user' => $user));
    }

}

<?php

class GroupPostLikesController extends \BaseController {

	/**
	 * Display a listing of grouppostlikes
	 *
	 * @return Response
	 */
	public function index()
	{
		$grouppostlikes = Grouppostlike::all();

		return View::make('grouppostlikes.index', compact('grouppostlikes'));
	}

	/**
	 * Show the form for creating a new grouppostlike
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('grouppostlikes.create');
	}

	/**
	 * Store a newly created grouppostlike in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Grouppostlike::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Grouppostlike::create($data);

		return Redirect::route('grouppostlikes.index');
	}

	/**
	 * Display the specified grouppostlike.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$grouppostlike = Grouppostlike::findOrFail($id);

		return View::make('grouppostlikes.show', compact('grouppostlike'));
	}

	/**
	 * Show the form for editing the specified grouppostlike.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$grouppostlike = Grouppostlike::find($id);

		return View::make('grouppostlikes.edit', compact('grouppostlike'));
	}

	/**
	 * Update the specified grouppostlike in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$grouppostlike = Grouppostlike::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Grouppostlike::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$grouppostlike->update($data);

		return Redirect::route('grouppostlikes.index');
	}

	/**
	 * Remove the specified grouppostlike from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Grouppostlike::destroy($id);

		return Redirect::route('grouppostlikes.index');
	}

}

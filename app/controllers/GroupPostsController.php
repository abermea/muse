<?php

class GroupPostsController extends \BaseController {

	/**
	 * Display a listing of groupposts
	 *
	 * @return Response
	 */
	public function index()
	{
		$groupposts = Grouppost::all();

		return View::make('groupposts.index', compact('groupposts'));
	}

	/**
	 * Show the form for creating a new grouppost
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('groupposts.create');
	}

	/**
	 * Store a newly created grouppost in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Grouppost::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Grouppost::create($data);

		return Redirect::route('groupposts.index');
	}

	/**
	 * Display the specified grouppost.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$grouppost = Grouppost::findOrFail($id);

		return View::make('groupposts.show', compact('grouppost'));
	}

	/**
	 * Show the form for editing the specified grouppost.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$grouppost = Grouppost::find($id);

		return View::make('groupposts.edit', compact('grouppost'));
	}

	/**
	 * Update the specified grouppost in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$grouppost = Grouppost::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Grouppost::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$grouppost->update($data);

		return Redirect::route('groupposts.index');
	}

	/**
	 * Remove the specified grouppost from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Grouppost::destroy($id);

		return Redirect::route('groupposts.index');
	}

}

<?php

class NoteCommentsController extends \BaseController {

	/**
	 * Display a listing of notecomments
	 *
	 * @return Response
	 */
	public function index()
	{
		$notecomments = Notecomment::all();

		return View::make('notecomments.index', compact('notecomments'));
	}

	/**
	 * Show the form for creating a new notecomment
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('notecomments.create');
	}

	/**
	 * Store a newly created notecomment in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Notecomment::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Notecomment::create($data);

		return Redirect::route('notecomments.index');
	}

	/**
	 * Display the specified notecomment.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$notecomment = Notecomment::findOrFail($id);

		return View::make('notecomments.show', compact('notecomment'));
	}

	/**
	 * Show the form for editing the specified notecomment.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$notecomment = Notecomment::find($id);

		return View::make('notecomments.edit', compact('notecomment'));
	}

	/**
	 * Update the specified notecomment in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$notecomment = Notecomment::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Notecomment::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$notecomment->update($data);

		return Redirect::route('notecomments.index');
	}

	/**
	 * Remove the specified notecomment from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Notecomment::destroy($id);

		return Redirect::route('notecomments.index');
	}

}

<?php

class NoteLikesController extends \BaseController {

	/**
	 * Display a listing of notelikes
	 *
	 * @return Response
	 */
	public function index()
	{
		$notelikes = Notelike::all();

		return View::make('notelikes.index', compact('notelikes'));
	}

	/**
	 * Show the form for creating a new notelike
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('notelikes.create');
	}

	/**
	 * Store a newly created notelike in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Notelike::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Notelike::create($data);

		return Redirect::route('notelikes.index');
	}

	/**
	 * Display the specified notelike.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$notelike = Notelike::findOrFail($id);

		return View::make('notelikes.show', compact('notelike'));
	}

	/**
	 * Show the form for editing the specified notelike.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$notelike = Notelike::find($id);

		return View::make('notelikes.edit', compact('notelike'));
	}

	/**
	 * Update the specified notelike in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$notelike = Notelike::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Notelike::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$notelike->update($data);

		return Redirect::route('notelikes.index');
	}

	/**
	 * Remove the specified notelike from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Notelike::destroy($id);

		return Redirect::route('notelikes.index');
	}

}

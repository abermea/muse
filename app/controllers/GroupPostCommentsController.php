<?php

class GroupPostCommentsController extends \BaseController {

	/**
	 * Display a listing of grouppostcomments
	 *
	 * @return Response
	 */
	public function index()
	{
		$grouppostcomments = Grouppostcomment::all();

		return View::make('grouppostcomments.index', compact('grouppostcomments'));
	}

	/**
	 * Show the form for creating a new grouppostcomment
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('grouppostcomments.create');
	}

	/**
	 * Store a newly created grouppostcomment in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Grouppostcomment::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Grouppostcomment::create($data);

		return Redirect::route('grouppostcomments.index');
	}

	/**
	 * Display the specified grouppostcomment.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$grouppostcomment = Grouppostcomment::findOrFail($id);

		return View::make('grouppostcomments.show', compact('grouppostcomment'));
	}

	/**
	 * Show the form for editing the specified grouppostcomment.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$grouppostcomment = Grouppostcomment::find($id);

		return View::make('grouppostcomments.edit', compact('grouppostcomment'));
	}

	/**
	 * Update the specified grouppostcomment in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$grouppostcomment = Grouppostcomment::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Grouppostcomment::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$grouppostcomment->update($data);

		return Redirect::route('grouppostcomments.index');
	}

	/**
	 * Remove the specified grouppostcomment from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Grouppostcomment::destroy($id);

		return Redirect::route('grouppostcomments.index');
	}

}

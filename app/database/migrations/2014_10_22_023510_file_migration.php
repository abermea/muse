<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FileMigration extends Migration {
    
    public static $table_name = 'file';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create(FileMigration::$table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->integer(UserTableMigrate::$table_name . '_id')->unsigned();
            $table->integer(ProyectoMigration::$table_name . '_id')->unsigned();
            $table->string('file_uri')->unique();
            $table->string('original_filename');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign(UserTableMigrate::$table_name . '_id')
                    ->references('id')
                    ->on(UserTableMigrate::$table_name);
            $table->foreign(ProyectoMigration::$table_name . '_id')
                    ->references('id')
                    ->on(ProyectoMigration::$table_name);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop(FileMigration::$table_name);
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNoteCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('NoteComments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('note_id')->unsigned();
                        $table->foreign('note_id')->references('id')->on('note');
			$table->integer('user_id')->unsigned();
                        $table->foreign('user_id')->references('id')->on('user');
			$table->text('content');
                        $table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('NoteComments');
	}

}

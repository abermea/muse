<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTableMigrate extends Migration {

    public static $table_name = 'user';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(UserTableMigrate::$table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->string('alias')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('first-name')->nullable();
            $table->string('last-name')->nullable();
            $table->string('message')->nullable();
            $table->string('avatar-uri')->nullable();
            $table->string('validation-code')->unique();
            $table->boolean('validated');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop(UserTableMigrate::$table_name);
    }

}

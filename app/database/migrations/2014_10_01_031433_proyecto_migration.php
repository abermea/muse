<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProyectoMigration extends Migration {
    
    public static $table_name = 'project';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create(ProyectoMigration::$table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->dateTime('start-date')->nullable();
            $table->dateTime('end-date')->nullable();
            $table->integer(UserTableMigrate::$table_name.'_id')->unsigned();
            $table->string('picture-uri')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign(UserTableMigrate::$table_name.'_id')
                    ->references('id')
                    ->on(UserTableMigrate::$table_name);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::drop(ProyectoMigration::$table_name);
    }

}

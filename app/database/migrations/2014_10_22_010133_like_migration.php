<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LikeMigration extends Migration {
    public static $table_name = 'like';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create(LikeMigration::$table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->integer(PostMigration::$table_name.'_id')->unsigned();
            $table->integer(UserTableMigrate::$table_name.'_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign(PostMigration::$table_name.'_id')
                    ->references('id')
                    ->on(PostMigration::$table_name);
            $table->foreign(UserTableMigrate::$table_name.'_id')
                    ->references('id')
                    ->on(UserTableMigrate::$table_name);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::drop(LikeMigration::$table_name);
    }

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupUserTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('groups_user', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('groups_id')->unsigned()->index();
            $table->foreign('groups_id')->references('id')->on('groups');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('user');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('group_user');
    }

}

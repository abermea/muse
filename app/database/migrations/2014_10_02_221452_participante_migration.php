<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParticipanteMigration extends Migration {


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        $table_name = 
            ProyectoMigration::$table_name.'_'.UserTableMigrate::$table_name;
        Schema::create($table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->integer(ProyectoMigration::$table_name.'_id')->unsigned();
            $table->integer(UserTableMigrate::$table_name.'_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign(UserTableMigrate::$table_name.'_id')
                    ->references('id')
                    ->on(UserTableMigrate::$table_name);
            $table->foreign(ProyectoMigration::$table_name.'_id')
                    ->references('id')
                    ->on(ProyectoMigration::$table_name);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        $table_name = 
            ProyectoMigration::$table_name.'_'.UserTableMigrate::$table_name;
        Schema::drop($table_name);
    }

}

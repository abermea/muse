<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityStatusMigration extends Migration {

    public static $table_name = 'activity_status';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create(ActivityStatusMigration::$table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::drop(ActivityStatusMigration::$table_name);
    }

}

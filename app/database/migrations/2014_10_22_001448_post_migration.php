<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostMigration extends Migration {

    public static $table_name = 'post';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create(PostMigration::$table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->integer(PostTypeMigration::$table_name.'_id')->unsigned();
            $table->integer(ProyectoMigration::$table_name.'_id')->unsigned();
            $table->string('data');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign(PostTypeMigration::$table_name.'_id')
                    ->references('id')
                    ->on(PostTypeMigration::$table_name);
            $table->foreign(ProyectoMigration::$table_name.'_id')
                    ->references('id')
                    ->on(ProyectoMigration::$table_name);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::drop(PostMigration::$table_name);
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityMigration extends Migration {

    public static $table_name = 'activity';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create(ActivityMigration::$table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer(ProyectoMigration::$table_name.'_id')->unsigned();
            $table->integer('manager')->unsigned();
            $table->integer('responsible')->unsigned();
            $table->integer('priority')->unsigned();
            $table->integer(ActivityStatusMigration::$table_name.'_id')->unsigned();
            $table->dateTime('begin_date');
            $table->dateTime('end_date');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('manager')->references('id')->on(UserTableMigrate::$table_name);
            $table->foreign('responsible')->references('id')->on(UserTableMigrate::$table_name);
            $table->foreign(ProyectoMigration::$table_name.'_id')->references('id')->on(ProyectoMigration::$table_name);
            $table->foreign(ActivityStatusMigration::$table_name.'_id')->references('id')->on(ActivityStatusMigration::$table_name);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::drop(ActivityMigration::$table_name);
    }

}

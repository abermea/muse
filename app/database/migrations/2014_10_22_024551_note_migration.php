<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NoteMigration extends Migration {

    public static $table_name = 'note';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create(NoteMigration::$table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->integer(UserTableMigrate::$table_name . '_id')->unsigned();
            $table->integer(ProyectoMigration::$table_name . '_id')->unsigned();
            $table->text('content');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign(UserTableMigrate::$table_name . '_id')
                    ->references('id')
                    ->on(UserTableMigrate::$table_name);
            $table->foreign(ProyectoMigration::$table_name . '_id')
                    ->references('id')
                    ->on(ProyectoMigration::$table_name);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::drop(NoteMigration::$table_name);
    }

}

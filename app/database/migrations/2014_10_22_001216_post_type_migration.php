<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostTypeMigration extends Migration {
    
    public static $table_name = 'post_type';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create(PostTypeMigration::$table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('template');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::drop(PostTypeMigration::$table_name);
    }

}

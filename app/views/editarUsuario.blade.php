@extends('base')

@section('contenido')
<?php
$data = json_decode($user, true);
?>
{{ Form::open(array('url' => '/api/user-update', 'id' => 'frm-edit-user', 'method' => 'POST')) }}

<input type="text" name="first-name" placeholder="Nombres" value="{{ $data['first-name'] }}">
<input type="text" name="last-name" placeholder="Apellidos" value="{{ $data['last-name'] }}">
<input type="text" name="alias" placeholder="Alias" value="{{ $data['alias'] }}">
<input type="text" name="email" placeholder="Correo electrónico" value="{{ $data['email'] }}">
<input type="password" name="old-password" placeholder="Tu antigua contraseña">
<input type="password" name="new-password" placeholder="Nueva contraseña">
<input type="text" name="message" placeholder="Breve descripción" value="{{ $data['message'] }}">
{{ Form::submit('Listo!') }}
{{ Form::close()}}

@if(Auth::check())
<a id='lnk-user-delete' href='{{route('main')}}/api/user-delete'>Eliminar cuenta</a>
@endif

@stop

@section('scripts')
<script>
    prepararFormEditarUsuario();
</script>
@stop
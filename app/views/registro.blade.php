@extends('dashboard')
@section('tab')
<!--
{{ Form::open(array('url' => 'api/complete-registration', 'id' => 'registro')) }}
    {{ Form::hidden('email', '', array('id' => 'email')) }}
    {{-- Form::hidden('validation-code', '', array('id' => 'validation-code')) --}}
    {{ Form::text('first-name', '', array('placeholder' => 'Nombres' ) ) }}
    {{ Form::text('last-name', '', array('placeholder' => 'Apellidos' ) ) }}
    <input type="password" name="password" placeholder="Contraseña">
    {{ Form::text('alias', '', array('placeholder' => 'Alias' ) ) }}
    {{ Form::text('message', '', array('placeholder' => 'Breve descripción' ) ) }}
    {{ Form::submit('Listo!') }}
    {{ Form::close()}} 

<a href="#" class="btn btn-default" data-toggle="modal" data-target="#basicModal">Iniciar Sesión</a>
-->

<div class="modal fade" id="modalRegis" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">Registro</h4>
            </div>
            <div class="modal-body">
                <span id="validando">Validando...</span>
                <div id="frm-registro" style="display: none;" >
                    <form action="api/complete-registration" id="registro">
                        <input type="hidden" name="email" id="email">
                        <input type="text" name="first-name" id="first-name" placeholder="Nombres"><br><br>
                        <input type="text" name="last-name" id="last-name" placeholder="Apellidos"><br><br>
                        <input type="password" name="password" placeholder="Contraseña"><br><br>
                        <input type="text" name="alias" id="alias" placeholder="Alias"><br><br>
                        <textarea name="message" id="message" placeholder="Breve descripción"></textarea>
                    </form>
                    <span id="verificando" style="display: none"></span>
                </div>
            </div>
            <div class="modal-footer">
                <div id="btn-regis" class="enviar">¡Listo!</div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
    $(function(){
        validarEmailRegistrado();
       prepararFormDeRegistroCompleta();
       
       $("#modalRegis").modal({
               backdrop: 'static'
           });
       
       $("#modalRegis").modal('show');
       
       $("#btn-regis").click(function(){
          $("#registro").submit(); 
       });
    });
</script>
@stop
@extends('dashboard')
@section('tab')
<div id="btn-invitar" class="enviar invitar">Invitar</div>
@include('InvitarUsuarios')
<div id="proyect">
    <ul>
        <li><a href="#proyect-1">Timeline</a></li>
        <li><a href="#proyect-3">Notas</a></li>
        <li><a href="#proyect-4">Archivos</a></li>
        <li><a href="#proyect-5" id="calenref">Calendario</a></li>
    </ul>
    <div id="proyect-1">
        <!-- INICIO DE TAB TIMELINE -->
        @include('timeline', array('project' => $project))
        <!-- FIN DE TAB TIMELINE -->
    </div>
    <!--    <div id="proyect-2">
             INICIO DE TAB EVENTOS 
            @include('crearEvento')
             FIN DE TAB EVENTOS 
        </div>-->
    <div id="proyect-3">
        <!-- INICIO DE TAB NOTAS -->
        <div class="scroller">
        @include('listaNotas', array('project' => $project))
        </div>
        <!-- FIN DE TAB NOTAS -->
    </div>
    <div id="proyect-4">
        <!-- INICIO DE TAB ARCHIVOS -->
        @include('listaArchivos', array('project' => $project))
        <!-- FIN DE TAB ARCHIVOS -->
    </div>
    <div id="proyect-5">
        <!-- INICIO DE TAB TAREAS -->
        @include('calendario')
        <!-- FIN DE TAB TAREAS -->
    </div>
</div>
@stop

@section('tasks')
<?php 
$tasks = API::get('api/activities-user-project', array('project' => $project)); ?>
@include('tasklist', array('tasks' => $tasks))
@stop

@section('scripts')
<script>
    $(function() {
        $("#proyect").tabs();
        
        $("#calenref").click(function(){
            $("#calendar").fullCalendar('removeEvents');
           renderTasks(); 
        });
    });
</script>
@stop
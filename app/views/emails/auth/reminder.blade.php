<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Recuperación de contraseña</h2>

                <div class="mensaje">
			Para que recuperes tu contraseña tendras que que llenar esta forma: {{ URL::to('password/reset', array($token)) }}.<br/>
			Este link va expirar en {{ Config::get('auth.reminder.expire', 60) }} minutos.
		</div>
	</body>
</html>

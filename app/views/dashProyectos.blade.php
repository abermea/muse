@extends('dashboard')
@section('tab')
<div id="scroller" class="scroller">
        <div id='btn-new-project' class="projectThumb">
            <div class="textProjectThumb">+<br/>Nuevo<br/>Proyecto</div>
        </div>
    <?php $projects = API::get('api/projects') ?>
    @foreach($projects as $project)
    <a href="{{route('main')}}/proyecto/{{$project['id']}}">
        <div class="projectThumb">
            <div class="textProjectThumb">{{$project['name']}}</div>
        </div>
    </a>
    @endforeach
</div>

@include('modals.crearProyecto')
<script>
    $(function(){
        
    });
</script>
@stop

@section('tasks')
<?php $tasks = API::get('api/activities-user'); ?>
@include('tasklist', array('tasks' => $tasks))
@stop

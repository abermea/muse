@if(Auth::check())
<div id="log-out">
<a id='lnk-logout' href=''>Salir</a>
</div>
<script>
    $('#lnk-logout').button().on('click', function(event) {
        var formURL = '<?php echo route('main') ?>/api/logout';
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    success: function(data, textStatus, jqXHR)
                    {
                        $(location).attr('href','<?php echo route('main') ?>');
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        
                    }
                });
        event.preventDefault(); //STOP default action
        event.unbind(); //unbind. to stop multiple form submit.
    });
</script>
@endif
<div class="scroller">
    <div class="containerTimeline">
        <!--Bloque -->
        <div class="postBlock">
            <div class="newPostContainer">
                <div class="newPost">
                    <textarea rows="2" cols="20" name="usrtxt" wrap="hard" placeholder="Escribe un nuevo post... "></textarea>
                    <div id="btn-new-post" class="enviar floatRight">Enviar</div>
                </div>
            </div>
        </div>
<?php $posts = API::get('/api/group-posts', array('group' => $group)) ?>

        @foreach($posts as $post)
        
            @include('post.postTimelineGrupo', array('post' => $post))
        
        @endforeach
       </div>
</div>

<script>
    $(function(){
        $('#btn-new-post').click(function () {
            $.ajax({
                url: '<?php echo route('main') ?>/api/group-post-create',
                type: 'POST',
                data: {
                    group: <?php echo $group ?>,
                    data: $('textarea[name="usrtxt"]').val(),
                },
                success: function () {
                    
                },
                error: function () {
                    
                }
            });
        });
    });
</script>
@extends('base')

@section('contenido')
    <fieldset>
        <legend>Registrarse</legend>
    {{ Form::open(array('url' => 'api/register' , 'id' => 'iniciando')) }}
    {{ Form::text('email', '', array('placeholder' => 'correo electronico' ) ) }}
    {{ Form::submit('Registrate') }}
    {{ Form::close() }}
    </fieldset>
    @include('login')
@stop

@section('scripts')
<script>
    prepararFormDeRegistro();
</script>
@stop
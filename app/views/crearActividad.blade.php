<?php $users = API::get('api/users-by-project', array('project' => $project)) ?>
<div id='frm-new-task' title="Nueva Tarea">
    <fieldset>
        {{Form::open(array('url' => 'api/activity-create', 'id' => 'nuevaTarea') )}}
        <input type="hidden" name="project" value="{{ $project }}"/>
        <input type="text" name="nombre" placeholder="Nombre de la Tarea"><br><br>
        <textarea name="desc" placeholder="Descripción" ></textarea><br><br>
        <input type="text" name="fechaInicio" id='fechaInicioAct' placeholder="Fecha de Inicio">
        <input type="text" name="horaInicio" id='horaInicioAct' placeholder="Hora de Inicio"><br><br>
        <input type="text" name="fechaFin" id='fechaFinAct' placeholder="Fecha de Fin">
        <input type="text" name="horaFin" id='horaFinAct' placeholder="Hora de Fin"><br><br>
        <select name="priority" id="priority" >
            <option value="-1">Seleccione un Nivel de Prioridad:</option>
            <option value="3">Baja</option>
            <option value="2">Media</option>
            <option value="1">Alta</option>
        </select><br>
        <select name="responsible" id="responsible" >
            <option value="-1">Seleccione un Usuario Responsable:</option>
            @foreach($users as $user)
            <option value="{{$user['id']}}">{{$user['first-name'].' '.$user['last-name']}}</option>
            @endforeach
        </select><br><br>
        {{Form::submit('Crear')}}
        {{Form::close()}}
    </fieldset>
</div>

<script>
    $(function() {
        var dialog;
        $("#fechaInicioAct").datepicker($.datepicker.regional[ 'es' ]);
        $("#fechaFinAct").datepicker($.datepicker.regional[ 'es' ]);
        $("#horaInicioAct").timepicker({'timeFormat': 'h:i A'});
        $("#horaFinAct").timepicker({'timeFormat': 'h:i A'});
        dialog = $("#frm-new-task").dialog({
            autoOpen: false,
            height: 420,
            width: 300,
            modal: true,
            buttons: {
            },
            close: function() {
                dialog.dialog('close');
            }
        });

        $("#btn-new-task").button().on('click', function(e) {
            dialog.dialog('open');
        });

        prepararNuevaTarea();
    });
</script>

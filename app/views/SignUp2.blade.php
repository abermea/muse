<html>
    <head>
        <title>Muse Dashboard</title>
        <script src="{{route('main')}}/script/jquery-2.1.1.min.js"></script>
        <script src="{{route('main')}}/script/jquery-ui/jquery-ui.min.js"></script>
        <script src="{{route('main')}}/script/jquery-validation/jquery.validate.min.js"></script>
        <script src="{{route('main')}}/script/muse.js"></script>
        <script src="{{route('main')}}/script/jquery-ui/datepicker-fr.js"></script>
        <script src="{{route('main')}}/script/jquery-ui/datepicker-es.js"></script>
        <script src="{{route('main')}}/script/jquery-ui/jquery.timepicker.js"></script>
        <script src="{{route('main')}}/script/jqueryfileup/js/jquery.iframe-transport.js"></script>
        <script src="{{route('main')}}/script/jqueryfileup/js/jquery.fileupload.js"></script>
        <script src="{{route('main')}}/script/fullcalendar-2.1.1/fullcalendar2.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <!--<link rel="stylesheet" href="{{route('main')}}/script/jquery-ui/jquery-ui.css">-->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{route('main')}}/script/jquery-ui/jquery.timepicker.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/bootstrap.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{route('main')}}/script/jqueryfileup/css/jquery.fileupload.css">
        <script src="{{route('main')}}/script/jquery-qtip/qtip2.js"></script>
        <link rel="stylesheet" href="{{route('main')}}/script/jquery-qtip/qtip2.css">
        <link rel="stylesheet" href="{{route('main')}}/script/fullcalendar-2.1.1/fullcalendar2.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/test.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museCore.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museDash.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museMsg.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museNotes.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museFiles.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museSignUp.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museTasks.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museText.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museTimelines.css">

    </head>
    <body>

        <div id="mainContainer">
            <div id="upperBar">

                <img id="imgLogo" src="{{route('main')}}/img/museLogo.png">
                <div id="userBlock">
                    <div style="float: right">
                        @include('modals.iniciarSesion')
                    </div>
                </div>
            </div>
            <div id="menu">
                <div class="sectionTitle"></div>

            </div>

            <div id="refresh">

                <div class="mainPicture">
                    <div class="signUpBox">
                        <div class="signUpContent">
                                <h2 style="color: lightgray">¡Registrate!</h2>
                                <input type="text" id="signup-username" placeholder="Correo electrónico">
                                <br><br>
                                <div id="btn-signup" class="enviar short">Registrarse</div><br>
                                <span id="enviando" style="display: none; color: #ddd;">Enviando...</span>
                        </div>
                    </div>

                </div>
                <div class="containerContent">
                    <div class="contLeft">

                        <div class="contentThird"><jmq>¿Qué es Muse?</jmq>
<br>
Muse es una herramienta diseñada especialmente para la gestión de proyectos de todo tipo. Pero lo más genial es que es muy sencilla de usar. Con Muse puedes organizar desde tus deberes escolares hasta ese enorme proyecto empresarial.</div></div>

                    <div class="contMid">
                        <div class="contentThird">
                            <jmq>¿Por qué usar Muse?</jmq>
<br>
¿Y por qué no? Es grandioso tener una herramienta que te facilite de manera tan simple todas las cosas que tienes que resolver para que tus proyectos salgan adelante de la manera más eficiente posible.

                        </div>

                    </div>

                    <div class="contRight">
                        <div class="contentThird">
                            <jmq>¿Quiénes somos?</jmq>
<br>
Somos un grupo de estudiantes universitarios con una misión filosófica única sobre la vida y el universo que nos llevó a desarrollar una aplicación del estilo de Muse con la intención de ayudar a la gente.
                        </div>
                    </div>
                </div>
            </div>

            <div id="footer"><a href="{{route('main')}}/copyright">Copyright</a> | <a href="{{route('main')}}/terminos">Términos y Condiciones</a> | <a href="{{route('main')}}/privacidad">Aviso de Privacidad</a> | <a href="{{route('main')}}/contacto">Contacto</a> | <a href="{{route('main')}}/faq">Preguntas Frecuentes</a> </div>
            <div class="dimmer"></div>
        </div>

        <script>
function attemptLogin(postData) {

    var formURL = '<?php echo route('main') ?>/api/authenticate';
    $.ajax({
        url: formURL,
        type: "POST",
        data: postData,
        success: function(data, textStatus, jqXHR) {
            $(location).attr('href', 'dashboard');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //$("#verificando").html('<br>' + textStatus + ': ' + errorThrown);
            $("#mensaje").text("Oops! Escribiste algo mal.");
        }
    });
}

function reenviarEmail() {
    $("#btn-reenviar").click(function(e) {
        var postData = {
            'email': $('#signup-username').val(),
        };
        $("#enviando").html('Enviando...').show();
        var formURL = $(this).attr("action");
        $.ajax({
            url: 'api/resend-email',
            type: "POST",
            data: postData,
            success: function(data, textStatus, jqXHR) {
                $("#enviando").html('Listo. <br>Te hemos enviado un correo para continuar <br>con el proceso de registro.<br> Si no lo has recibido aún haz click en el botón: <br><div id="btn-reenviar" class="enviar short">Reenviar correo</div>');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#enviando").html('Error');
            }
        });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function recuperar() {
    $("#btn-recuperar").click(function(e) {
        var postData = {
            'email': $('#signup-username').val(),
        };
        $("#enviando").html('Enviando...').show();
        var formURL = $(this).attr("action");
        $.ajax({
            url: 'api/password-retrieval',
            type: "POST",
            data: postData,
            success: function(data, textStatus, jqXHR) {
                $("#enviando").html('Tu correo ya esta registrado con nosotros. Presiona este botón para recuperar tu contraseña: <div id="btn-recuperar" class="enviar short">Reenviar correo</div>');
                recuperar();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#enviando").html('Error');
            }
        });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

$(function() {

    $('#btn-signup').button().on('click', function(event) {
        $("#enviando").html('Enviando...').show();
        var postData = {
            'email': $('#signup-username').val()
        };
        var formURL = 'api/register';
        $.ajax({
            url: formURL,
            type: "POST",
            data: postData,
            success: function(data, textStatus, jqXHR) {
                $("#enviando").html('Listo. <br>Te hemos enviado un correo para continuar <br>con el proceso de registro.<br> Si no lo has recibido aún haz click en el botón: <br><div id="btn-reenviar" class="enviar short">Reenviar correo</div>');
                reenviarEmail();

            },
            error: function(jqXHR, textStatus, errorThrown) {
                if(errorThrown === 'Conflict') {
                    $("#enviando").html('Tu correo ya esta registrado con nosotros. Presiona este botón para recuperar tu contraseña: <div id="btn-recuperar" class="enviar short">Reenviar correo</div>');
                    recuperar();
                } else {
                    $("#enviando").html('No fue posible registrarte en este momento. Intenta de nuevo mas tarde.');
                }
            }
        });
        event.preventDefault(); //STOP default action
        event.unbind(); //unbind. to stop multiple form submit.
    });
});
        </script>
    </body>
</html>









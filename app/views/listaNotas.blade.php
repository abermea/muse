<?php
$notas = API::get('api/notes', array('project' => $project));
?>
<script>
    function prepareDeleteNote(count) {
        var id = '#lnk-delete-note-' + count;
        var noteId = $("#lnk-delete-note-id-" + count).val();
        $(id).button().on('click', function(e) {
            ohSnap('Eliminando nota...','yellow');
            $.ajax({
                url: '<?php echo route('main') ?>/api/note-delete',
                type: "POST",
                data: {
                    note: noteId
                },
                success: function(data, textStatus, jqXHR)
                {
                    $("#row-note-" + count).remove();
                    ohSnapX();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    var a = 0;
                }
            });
            e.preventDefault(); //STOP default action
        });
    }
    
    function traerNota(valor){
    $("#mensaje").text('Cargando nota...');
    $("#alertNota").show();
           $.ajax(
                {
                    url: '{{route('main')}}/api/note',
                    type: "GET",
                    data: {
                        'note': $('#show-note-id-'+valor).val()
                    },
                    success: function(data, textStatus, jqXHR)
                    {
                        $("#alertNota").hide();
                        $('#note-title').val(data.name);
                        $('#contenido').val(data.content);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $("#mensaje").text("Error: " + errorThrown);
                    }
                });
       }
   
        
</script>

<div id="ohsnap"></div>
<table id="tableFiles">
    <tr>
        <th colspan="2">
            Notas
    <div id="btn-new-note" class="enviar floatRight">Nueva</div>
</th>
</tr>
<?php $count = 0 ?>
@foreach($notas as $nota) 
<?php $count+=1; ?>
<tr id="row-note-{{ $count }}">
    <td>
        <input id="show-note-id-{{ $count }}" type="hidden" value="{{$nota['id']}}"/>
        <a id="lnk-show-note-id-{{ $count }}">{{ $nota['name']  }}</a>
    </td>
    <td>
        <div id="lnk-delete-note-{{ $count }}" class="close"><span class="glyphicon glyphicon-remove"></span></div>
    </td>
</tr>
<script>
    //crearEventoTraerNota({{$count}});
</script>
    

@endforeach
</table>

@include('modals.vistaNota', array('project' => $project))
@include('modals.crearNota', array('project' => $project))



<script>
    $(function() {
        
        var dialog, form;
        function closeDialog() {
            dialog.dialog('close');
        }

        for(var i = 1; i <= <?php echo $count ?>; i++){
            prepareDeleteNote(i);
            $("#row-note-"+i).addClass('imahand');
            abrirModalNota(i);
        }

        dialog = $("#modal-create-note").dialog({
            autoOpen: false,
            height: 450,
            width: 350,
            modal: true,
            buttons: {
            },
            close: function() {
                closeDialog();
            }
        });
        
        $('#btn-new-note').button().on('click', function() {
            $("#modalCrearNota").modal('show');
        });
        
    });
    
    function abrirModalNota(index){
         $('#row-note-'+index).button().on('click', function() {
             $('#note-title').val("");
             $('#contenido').val("");
             traerNota(index);
            $("#modalVistaNota").modal('show');
        });
    }
</script>


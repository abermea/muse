<html>
    <head>
        <title>Muse</title>
        <script src="{{route('main')}}/script/jquery-2.1.1.min.js"></script>
        <script src="{{route('main')}}/script/jquery-ui/jquery-ui.min.js"></script>
        <script src="{{route('main')}}/script/jquery-validation/jquery.validate.min.js"></script>
        <script src="{{route('main')}}/script/muse.js"></script>
        <script src="{{route('main')}}/script/jquery-ui/datepicker-es.js"></script>
        <script src="{{route('main')}}/script/jquery-ui/jquery.timepicker.js"></script>
        <script src="{{route('main')}}/script/jqueryfileup/js/jquery.iframe-transport.js"></script>
        <script src="{{route('main')}}/script/jqueryfileup/js/jquery.fileupload.js"></script>
        <script src="{{route('main')}}/script/fullcalendar-2.1.1/fullcalendar2.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="{{route('main')}}/script/ohsnap.js"></script>
        <!--<link rel="stylesheet" href="{{route('main')}}/script/jquery-ui/jquery-ui.css">-->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{route('main')}}/script/jquery-ui/jquery.timepicker.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{route('main')}}/script/jqueryfileup/css/jquery.fileupload.css">
        <script src="{{route('main')}}/script/jquery-qtip/qtip2.js"></script>
        <link rel="stylesheet" href="{{route('main')}}/script/jquery-qtip/qtip2.css">
        <link rel="stylesheet" href="{{route('main')}}/script/fullcalendar-2.1.1/fullcalendar2.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/test.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museCore.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museDash.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museMsg.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museNotes.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museFiles.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museSignUp.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museTasks.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museText.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museTimelines.css">
        <link rel="stylesheet" href="{{route('main')}}/CSS/ohSnap.css">
        <link rel="icon" type="image/png" href="{{route('main')}}/img/MuseFav.png"/>
    </head>
    <body>
        @yield('contenido')
        @yield('scripts')
        <script>
            prepararControlesUsuario();
        </script>
    </body>
</html>


<?php
$files = API::get('api/files-project', array('project' => $project));
?>
<script>
    function prepareDeleteFile(count) {
        var id = '#lnk-delete-file-' + count;
        var fileId = $("#lnk-file-id-" + count).val();
        $(id).button().on('click', function(e) {
            $.ajax({
                url: '<?php echo route('main') ?>/api/file-delete',
                type: "POST",
                data: {
                    file: fileId,
                },
                success: function(data, textStatus, jqXHR)
                {
                    $("#row-file-" + count).remove();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    var a = 0;
                }
            });
            e.preventDefault(); //STOP default action
        });
    }
    
    function prepareDownloadFile(count) {
        var id = '#lnk-file-download-' + count;
        var fileId = $("#lnk-file-id-" + count).val();
        $(id).button().on('click', function(e) {
            $.ajax({
                url: '<?php echo route('main') ?>/api/file',
                type: "GET",
                data: {
                    file: fileId,
                },
                success: function(data, textStatus, jqXHR)
                {
                    
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    var a = 0;
                }
            });
            e.preventDefault(); //STOP default action
        });
    }
</script>
<table id="tableFiles">
    <tr>
        <th colspan="2">
            Archivos
    <div id="btn-new-file" class="enviar floatRight">Nuevo</div>
</th>
</tr>
<?php $count = 0 ?>
@foreach($files as $file) 
<?php $count+=1; ?>
<tr id="row-file-{{ $count }}">

    <td id="lnk-file-download-{{ $count }}">
        <input id="lnk-file-id-{{ $count }}" type="hidden" value="{{$file['id']}}"/>
        <a href="{{route('main')}}/api/file?file={{$file['id']}}">{{ $file['original_filename']  }}</a>
    </td>
    <td><div id="lnk-delete-file-{{ $count }}">x</div></td>
</tr>
@endforeach
</table>


@include('subirArchivo', array('project' => $project))

<script>
    $(function() {
        for(var i = 1; i <= <?php echo $count ?>; i++){
            prepareDeleteFile(i);
            //prepareDownloadFile(i);
        }
    });
</script>



<div class="modal fade" id="modalInvitar" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">Invitar usuarios</h4>
            </div>
            <div class="modal-body">
                <!-- form invitaciones -->
                <div id="frm-invitaciones">
                    {{Form::open(array('url' => 'api/invite', 'id' => 'invitar')) }}
                    <div id="add" class="enviar floatLeft noMargin">Agregar</div><br><br>
                    <div class="correos">
                        <input type="text" name="email[]" placeholder="ejemplo@correo.com">
                    </div>
                    <input type="hidden" name="" value="{{ $project }}">
                    {{Form::close() }}
                </div>
                <!-- form invitaciones -->
                <div id="alertInvitar" class="alert alert-warning" style="display: none;">
                    <a href="#" class="close">&times;</a>
                    <span id="mensaje">...</span>
                </div>
            </div>
            <div class="modal-footer">
                <div id="btn-invitando" class="enviar">Invitar</div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        prepararInvitarUsuarios();
        
        
    $(".close").click(function(){
        $("#alertInvitar").hide();
    });
        
        $("#add").click(function() {
            $(".correos").append('<div><br><input type="text" name="email[]" placeholder="ejemplo@correo.com">'
                    + '<div class="removerCorreo quitar"><span class="glyphicon glyphicon-remove"></span></div></div>');
        });
        $(".correos").on("click", ".quitar", function() {
            $(this).parent('div').remove();
        });
        
        $("#btn-invitar").click(function(){
            $("#modalInvitar").modal('show');
        }); 
        
        $("#btn-invitando").click(function(){
           $("#alertInvitar").removeClass('alert-danger').addClass('alert-warning');
           $("#invitar").submit(); 
        });
    });
</script>
@extends('dashboard')

@section('tab')
<div id="scroller" class="scroller">
        <div id='btn-new-group' class="projectThumb">
            <div class="textProjectThumb">+<br/>Nuevo<br/>Grupo</div>
        </div>
    <?php $groups = API::get('api/groups') ?>
    <?php Log::info($groups) ?>
    @foreach($groups as $group)
    <a href="grupo/{{$group['id']}}">
        <div class="projectThumb">
            <div class="textProjectThumb">{{$group['name']}}</div>
        </div>
    </a>
    @endforeach
</div>
@include('modals.crearGrupo')
@stop



@section('tasks')
<?php $tasks = API::get('api/activities-user'); ?>
@include('tasklist', array('tasks' => $tasks))
@stop

@section('scripts')
<script>
    $(function(){
       $("#btn-new-group").click(function(){
           $("#modalCrearGrupo").modal('show');
       }) ;
    });
</script>
@stop
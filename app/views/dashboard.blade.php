@extends('base')
@section('contenido')
<!--Contiene todo-->
<div id="mainContainer">

    <!--Frame superior-->
    <div id="upperBar">
        <a href="{{route('main')}}/dashboard"><img id="imgLogo" src="{{route('main')}}/img/MuseLogo.png"></img></a>

        <!--Bloque de usuario-->
        @include('controlesUsuario')
        @include('modals.perfilUsuario')
        <div id="userBlock">
            <a href="{{route('main')}}/dashboard"><div id="btnProyecto"></div></a>
            <a href="{{route('main')}}/grupos"><div id="btnGpo"></div></a>
            <a href="{{route('main')}}/mensajes"><div id="btnMsg"></div></a>
            <div id="userPic"></div>
        </div>
    </div>

    <!--Titulo de la sección-->
    <div id="menu">
        <div class="sectionTitle">Dashboard</div>
    </div>

    <div id="refresh">
        <div id="leftContainer">
            <div id="fullContainer">
                @yield('tab')
            </div>
        </div>
        <div id="rightContainer">
            @yield('tasks')
        </div>
    </div>
    <div id="footer"><a href="{{route('main')}}/copyright">Copyright</a> | <a href="{{route('main')}}/terminos">Términos y Condiciones</a> | <a href="{{route('main')}}/privacidad">Aviso de Privacidad</a> | <a href="{{route('main')}}/contacto">Contacto</a> | <a href="{{route('main')}}/faq">Preguntas Frecuentes</a></div>
    <div class="dimmer"></div>
</div>
@stop

@section('scripts')
@yield('scripts')
@stop

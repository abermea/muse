<div class="scroller">
    <div class="containerTimeline">
        <!--Bloque -->
        <div class="postBlock">
            <div class="newPostContainer">
                <div class="newPost">
                    <textarea rows="2" cols="20" name="usrtxt" wrap="hard" placeholder="Escribe un nuevo post... "></textarea>
                    <div id="btn-new-post" class="enviar floatRight">Enviar</div>
                </div>
            </div>
        </div>
<?php $posts = API::get('/api/project-post', array('project' => $project)) ?>
        <!--Bloque -->
<!--        <div class="postBlock">
            <div class="post">
                <div class="avatar">
                    Profile
                </div>
                <div class="news">
                    <a href="">iorX L'Mad</a> ha compartido un <a href="">enlace</a> en el grupo.
                    <br>
                    <date>Hace 6 hrs.</date>
                </div>
                <div class="interactions">
                    <a href="">Comentar</a> | <a href="">Like</a> 
                </div>
            </div>

            <div class="commentBox">
                <div class="commentAvatar"></div>
                <div class="commentText">
                    <a href="">Adrian Bermea</a> comentó:<br>
                    Pinche Karina.<br>
                    <date>Hace 2 hrs</date>
                </div>
            </div>
        </div>-->
        @foreach($posts as $post)
        
            <?php $type = API::get('api/post-type', array('type' => $post['post_type_id'])) ?>
            @include($type['template'], array('data' => $post['data'], 'post_id' => $post['id']))
        
        @endforeach
        <!--Bloque -->
                <!--Bloque -->
<!--        <div class="postBlock">
            <div class="post">
                <div class="avatar">
                    Profile
                </div>
                <div class="news">
                    <a href="">iorX L'Mad</a> ha compartido un <a href="">enlace</a> en el grupo.
                    <br>
                    <date>Hace 6 hrs.</date>
                </div>
                <div class="interactions">
                    <a href="">Comentar</a> | <a href="">Like</a>
                </div>
            </div>

            <div class="commentBox">
                <div class="commentAvatar"></div>
                <div class="commentText">
                    <a href="">Adrian Bermea</a> comentó:<br>
                    Pinche Karina.<br>
                    <date>Hace 2 hrs</date>
                </div>
            </div>
        </div>-->
        <!--Bloque -->

    </div>
</div>

<script>
    $(function(){
        $('#btn-new-post').click(function () {
            $.ajax({
                url: '<?php echo route('main') ?>/api/post-create',
                type: 'POST',
                data: {
                    project: <?php echo $project?>,
                    content: $('textarea[name="usrtxt"]').val(),
                },
                success: function () {
                    
                },
                error: function () {
                    
                }
            });
        });
    });
</script>
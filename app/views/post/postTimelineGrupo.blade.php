   <?php
   try {
    Log::info($post);
    //$decode = json_decode($post, true);
    //Log::info($decode);

    //$user = API::get('/api/user', array('alias' => $decode['user_id']));
    //$content = $decode['data'];
    $user = API::get('/api/user', array('alias' => $post['user_id']));
    $content = $post['data'];
?>
<div>
<div class="postBlock">
    <div class="post">
    <!--    <div class="avatar">
            Profile
        </div>-->
    <div class="news">
        <a href="">{{ $user['first-name'].' '.$user['last-name'] }}</a> hizo una publicación:
        <br>
        <div>
            {{ $content }}
        </div>
        <!--<date>Hace 6 hrs.</date>-->
    </div>
    <div class="interactions">
        <!--<a href="">Comentar</a> |-->
        <a id="lnk-like-post-<?php echo $post['id'] ?>">Like</a> 
        <a id="lnk-unlike-post-<?php echo $post['id'] ?>" style="display: none;">Unlike</a> 
    </div>
</div>
</div>
<script>
    $(function() {
        $('#lnk-like-post-<?php echo $post['id'] ?>').button().on('click', function(event) {
            $.ajax({
                url: '<?php echo route('main') ?>/api/like-create',
                type: 'POST',
                data: {
                    post: '<?php echo $post['id'] ?>'
                },
                success: function(data, textStatus, jqXHR)
                {
                    $("#lnk-like-post-<?php echo $post['id'] ?>").hide();
                    $("#lnk-unlike-post-<?php echo $post['id'] ?>").show();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    
                }
            });
        });
        
        $('#lnk-unlike-post-<?php echo $post['id'] ?>').button().on('click', function(event) {
            $.ajax({
                url: '<?php echo route('main') ?>/api/like-delete',
                type: 'POST',
                data: {
                    post: '<?php echo $post['id'] ?>'
                },
                success: function(data, textStatus, jqXHR)
                {
                    $("#lnk-like-post-<?php echo $post['id'] ?>").show();
                    $("#lnk-unlike-post-<?php echo $post['id'] ?>").hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    
                }
            });
        });
    });
</script>
</div>
<?php 
} catch(Exception $e) {
    ?> 
    <?php
} finally {
    ?> <?php
    }
?>


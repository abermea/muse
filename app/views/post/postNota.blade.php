<?php
try {
    $decode = json_decode($data, true);
$user = API::get('/api/user', array('alias' => $decode['user']));
$note = API::get('/api/note', array('note' => $decode['note']));
?>

<div class="postBlock">
    <div class="post">
<!--    <div class="avatar">
        Profile
    </div>-->
    <div class="news">
        <a href="">{{ $user['first-name'].' '.$user['last-name'] }}</a> añadió una nota: <br/>
        {{ $note['content'] }}
        <br>
        <!--<date>Hace 6 hrs.</date>-->
    </div>
    <div class="interactions">
        <!--<a href="">Comentar</a> |--> 
        <a id='lnk-like-post-{{$post_id}}'>Like</a> 
        <a id='lnk-unlike-post-{{$post_id}}' style="display: none;">Unlike</a> 
    </div>
</div>
</div>
<script>
    $(function() {
        $('#lnk-like-post-<?php echo $post_id ?>').button().on('click', function(event) {
            $.ajax({
                url: '<?php echo route('main') ?>/api/like-create',
                type: 'POST',
                data: {
                    post: '<?php echo $post_id ?>'
                },
                success: function(data, textStatus, jqXHR)
                {
                    $("#lnk-like-post-<?php echo $post_id ?>").hide();
                    $("#lnk-unlike-post-<?php echo $post_id ?>").show();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    
                }
            });
        });
        
        $('#lnk-unlike-post-<?php echo $post_id ?>').button().on('click', function(event) {
            $.ajax({
                url: '<?php echo route('main') ?>/api/like-delete',
                type: 'POST',
                data: {
                    post: '<?php echo $post_id ?>'
                },
                success: function(data, textStatus, jqXHR)
                {
                    $("#lnk-like-post-<?php echo $post_id ?>").show();
                    $("#lnk-unlike-post-<?php echo $post_id ?>").hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    
                }
            });
        });
    });
</script>
<?php } catch(Exception $e) {
    ?> <?php
} finally {
    ?> <?php
    }
?>
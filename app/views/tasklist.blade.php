@foreach($tasks as $task)
<?php $project = API::get('api/project', array('project' => $task['project_id'] )); 

    $prioridad = $task['priority'];
    $claseP = "";
    
      if($prioridad == 1){
          $claseP = "taskIconBajo";
    }else if($prioridad == 2){
        $claseP = "taskIconMedio";
    }else if($prioridad == 3){
        $claseP = "taskIconAlto";
    }
?>
<div class="task">
    <div class="taskIcon {{ $claseP }}"></div>
    <div class="taskText">
        <div class="txt1">
            {{ $task['name'] }}
        </div>
        <br>
        <div class="txt2">
            {{ $project['name'] }}
        </div >
    </div>
</div>
@endforeach
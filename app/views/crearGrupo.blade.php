<?php 
 //$data = json_decode($user, true);
?>
<div id='frm-newGroup'>
    <fieldset>
    <legend>Nuevo Grupo</legend>
    {{Form::open(array('url' => 'api/group-create', 'id' => 'nuevoGrupo') )}}
    <input type="text" name="nombre" id="nombre" placeholder="Nombre del grupo">
    <input type="text" name="desc" id="desc" placeholder="Descripción">
    {{Form::submit('Crear')}}
    {{Form::close()}}
    </fieldset>
</div>

<script>
$(function() {
     prepararNuevoGrupo();
	
	 dialog = $("#frm-newGroup").dialog({
            autoOpen: false,
            height: 200,
            width: 300,
            modal: true,
            buttons: {
            },
            close: function() {
                dialog.dialog('close');
            }
        });
});
</script>

@extends('dashboard')
@section('tab')
<div id="btn-invitar" class="enviar invitar">Invitar</div>
@include('InvitarUsuariosGrupos')
<div id="proyect">
    @include('timelineGrupo', array('group' => $group))
</div>
@stop

@section('tasks')
<?php $users = API::get('api/users-by-group', array('group'=>$group)) ?>
@include('userlist', array('users' => $users))
@stop

@section('scripts')
@stop
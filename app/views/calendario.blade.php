<?php
$files = API::get('api/files-project', array('project' => $project));
?>
<div class="scroller">
    <div id="eventos">
        <div id="btn-new-task" class="enviar floatLeft">Nueva Tarea</div>
        <br><br><br>
        <div id="calendar"></div>
    </div>
    @include('modals.crearTarea')
</div>
<script>
    $(function() {
        prepararNuevaTarea();
        
        $("#btn-new-task").click(function(){
            $("#modalTarea").modal('show');
        });
        window.renderTasks();
        $("#calendar").fullCalendar({
            header: {
                left: 'prev,today',
                center: 'title',
                right: 'today,next'
            },
            dayClick: function(date, jsEvent, view) {

            },
            eventClick: function(event, jsEvent, view) {

            },
            eventRender: function(event, element) {
                element.css('cursor', 'pointer');
            }
        });
    });
    
    function renderTasks(){
              $.ajax({
           url: '<?php echo route('main') ?>/api/activities-user-project',
           type: 'GET',
           data: {
               project: <?php echo $project ?>
           },
           success: function(data, textStatus, jqXHR)
                    {
                        $(data).each(function(index){
                            var event = renderEvent(this);
                            $("#calendar").fullCalendar('renderEvent', event);
                        });
                    },
           error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('ah >:C');
                    }
        });  
    };
</script>



<div id="frm-login" >
    <fieldset>
        <legend>Iniciar Sesión</legend>
    {{ Form::open(array('url' => 'api/authenticate', 'id' => 'login')) }}
    {{ Form::text('alias', '', array('placeholder' => 'Alias o Correo electrónico' ) ) }}
    <input type="password" placeholder="Contraseña">
    <label for="remember-me">Recuerdame en este equipo</label>
    <!--<input type="checkbox" id="remember-me" name="remember-me"/> -->
    {{ Form::checkbox('remember-me', '', false, array('id' => 'remember-me')) }}
    {{ Form::submit('Entrar') }}
    {{ Form::close()}}
    <a href="">Recuperar contraseña</a>
    </fieldset>
</div>

<script>
    prepararFormInicioSesion();
</script>
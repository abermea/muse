<?php 
 //$data = json_decode($user, true);
?>
<div id='frm-newEvent'>
    <fieldset>
    <legend>Nuevo Evento</legend>
    {{Form::open(array('url' => 'api/appointment-create', 'id' => 'nuevoEvento') )}}
    <input type="hidden" name="project" value="4"/>
    <input type="text" name="nombre" id="nombre" placeholder="Nombre del evento"><br><br>
    <textarea name="desc" id="desc" placeholder="Descripción" ></textarea><br><br>
    <input type="text" name="fechaInicio" id="fechaInicio" placeholder="Fecha de Inicio">
    <input type="text" name="fechaFin" id="fechaFin" placeholder="Fecha de Fin"><br><br>
    <input type="text" name="horaInicio" id="horaInicio" placeholder="Hora de Inicio">
    <input type="text" name="horaFin" id="horaFin" placeholder="Hora de Fin"><br><br>
    <input type="text" name="lugar" id="lugar" placeholder="Lugar"><br><br>
    {{Form::submit('Crear')}}
    {{Form::close()}}
    </fieldset>
</div>

<script>
    $(function(){        
     $("#fechaInicio").datepicker($.datepicker.regional[ 'es' ]);
     $("#fechaFin").datepicker($.datepicker.regional[ 'es' ]);
     $("#horaInicio").timepicker({'timeFormat':'h:i A'});
     $("#horaFin").timepicker({'timeFormat':'h:i A'});
     prepararNuevoEvento();
    });
</script>

<div id='frm-newProyect' title='Nuevo Proyecto'>
    <fieldset>
        {{Form::open(array('url' => 'api/project-create', 'id' => 'nuevoProyecto') )}}
        <input type="text" name="titulo" id="titulo" placeholder="Título"><br/>
        <input type="text" name="desc" id="desc" placeholder="Descripción"><br/>
        <input type="text" name="fechaInicio" id="fechaInicio" placeholder="Fecha de Inicio"><br/>
        <input type="text" name="fechaFin" id="fechaFin" placeholder="Fecha de Fin"><br/>
        {{Form::submit('Crear')}}
        {{Form::close()}}
    </fieldset>
</div>

<script>
    $(function() {
        var dialog;
        $("#fechaInicio").datepicker($.datepicker.regional[ 'es' ]);
        $("#fechaFin").datepicker($.datepicker.regional[ 'es' ]);

        prepararNuevoProyecto();
        
        dialog = $("#frm-newProyect").dialog({
            autoOpen: false,
            height: 200,
            width: 300,
            modal: true,
            buttons: {
            },
            close: function() {
                dialog.dialog('close');
            }
        });
        /*
        $('#btn-new-project').button().on('click',function(event) {
            event.preventDefault();
            dialog.dialog('open');
        });*/
    });
</script>

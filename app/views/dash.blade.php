<html ng-app="PruebaMuse">
    <head>
        <title>Muse</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script src="js/moment.js"></script>
        <script src="js/fullcalendar/fullcalendar.js"></script>
        <script src="js/fullcalendar/gcal.js"></script>
        <script src="js/ui-bootstrap-tpls-0.12.0.js" type="text/javascript"></script>
        <script src="js/my-app.js" type="text/javascript"></script>
        <script src="js/project-controller.js" type="text/javascript"></script>
        <script src="js/calendar.js"></script>
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
        <link rel="stylesheet" href="js/fullcalendar/fullcalendar.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/museCore.css">
        <link rel="stylesheet" href="css/museDash.css">
        <link rel="stylesheet" href="css/museMsg.css">
        <link rel="stylesheet" href="css/museNotes.css">
        <link rel="stylesheet" href="css/museFiles.css">
        <link rel="stylesheet" href="css/museSignUp.css">
        <link rel="stylesheet" href="css/museTasks.css">
        <link rel="stylesheet" href="css/museText.css">
        <link rel="stylesheet" href="css/museTimelines.css">
    </head>
    <body>
        <div id="mainContainer">
            <!--Frame superior-->
            <div id="upperBar">
                <a href=""><img id="imgLogo" src="img/museLogo.png"></img></a>
                <!--Bloque de usuario-->
                <div id="userBlock">
                    <a href="{{route('main')}}/dashboard"><div id="btnProyecto"></div></a>
                    <a href="{{route('main')}}/grupos"><div id="btnGpo"></div></a>
                    <a href="{{route('main')}}/mensajes"><div id="btnMsg"></div></a>
                    <div id="userPic"></div>
                </div>
            </div>
            <!--Titulo de la sección-->
            <div id="menu">
                <div class="sectionTitle">
                    Dashboard
                </div>
            </div>
            <div id="refresh" class="" ng-controller="fullController">
                <div id="leftContainer">
                    <div id="fullContainer">
                        <div id="scroller" class="scroller" ng-model="projects.list">
                            <div id="project-list" ng-show="!inProject">
                                <a href="">
                                    <div id='btn-new-project' class="projectThumb">
                                        <div style="font-size: 24px; text-align: center;position: relative;top: 50%;left:50%;transform: translate(-50%,-50%);">+<br/>Nuevo<br/>Proyecto</div>
                                    </div>
                                </a>
                                <a href="" ng-repeat="project in project.list" ng-click="selectProject(project)">
                                    <div class="projectThumb">
                                        <div style="font-size: 24px; text-align: center;position: relative;top: 50%;left:50%;transform: translate(-50%,-50%);" ng-bind="project.name"></div>
                                    </div>
                                </a>
                            </div>
                            <div ng-controller="projectTabsController" ng-show="inProject">
                                <tabset justified="true">
                                    <tab>
                                        <tab-heading>
                                            <i class="glyphicon glyphicon-chevron-left"></i> Inicio
                                        </tab-heading>
                                    </tab>
                                    <tab>
                                        <tab-heading>
                                            <i class="glyphicon glyphicon-pushpin"></i> Muro
                                        </tab-heading>
                                        <div class="postBlock">
                                            <div class="newPostContainer">
                                                <div class="newPost">
                                                    <textarea rows="2" cols="20" name="usrtxt" wrap="hard" placeholder="Escribe un nuevo post... "></textarea>
                                                    <div class="enviar">Enviar</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="postBlock">
                                        </div>
                                    </tab>
                                    <tab>
                                        <tab-heading>
                                            <i class="glyphicon glyphicon-list-alt"></i> Notas
                                        </tab-heading>
                                        <table ng-controller="noteController">
                                            <thead>
                                                <tr>
                                                    <td colspan="2">Notas</td>
                                                    <td ng-click="create('lg')">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="note in project.notes">
                                                    <td ng-bind="note.content">
                                                    </td>
                                                    <td>
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </td>
                                                    <td>
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </tab>
                                    <tab>
                                        <tab-heading>
                                            <i class="glyphicon glyphicon-folder-open"></i> Archivos
                                        </tab-heading>
                                        <table>
                                            <tr ng-repeat="file in project.files">
                                                <td ng-bind="file.original_filename">
                                                </td>
                                                <td>
                                                    <i class="glyphicon glyphicon-remove"></i>
                                                </td>
                                            </tr>
                                        </table>
                                    </tab>
                                    <tab ng-controller="calendarController">
                                        <tab-heading>
                                            <i class="glyphicon glyphicon-calendar"></i> Calendario
                                        </tab-heading>
                                        <div>
                                            <div class="calendar" ng-model="eventSources" calendar="projectTaskCalendar" ui-calendar="uiConfig.calendar"></div>
                                        </div>
                                    </tab>
                                </tabset>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="rightContainer">
                    <div ng-model="project">
                        <div class="task" ng-repeat="task in project.tasks | filter:{project_id : project.selected.id} | orderBy:priority">
                            <div>
                                <div class="taskIcon">
                                </div>
                                <div class="taskText">
                                    <div class="txt1" ng-bind="task.name">
                                    </div>
                                    <br>
                                    <div class="txt2">
                                    </div >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="footer">
                <a href="{{route('main')}}/copyright">Copyright</a> | <a href="{{route('main')}}/terminos">Términos y Condiciones</a> | <a href="{{route('main')}}/privacidad">Aviso de Privacidad</a> | <a href="{{route('main')}}/contacto">Contacto</a> | <a href="{{route('main')}}/faq">Preguntas Frecuentes</a>
            </div>
            <div class="dimmer"></div>
        </div>
    </body>
</html>
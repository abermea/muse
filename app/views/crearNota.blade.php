
<div id='frm-newNotes'>
    <fieldset>
    {{Form::open(array('url' => 'api/note-create', 'id' => 'nuevaNota') )}}
    <input type="text" name="name" value="" placeholder="Nombre"/></br>
    <textarea style="width: 100%; height:90%;" name="contenido" id="contenido" placeholder="Escribe tu nota" wrap="hard"></textarea><br/>
    {{Form::hidden('idUser', Auth::user()->id, array('id' => 'idUser')) }}
    {{Form::hidden('idProject', $project, array('id' => 'idProject')) }}
    <input style="float: right;" type="submit" value="Crear"/>
    {{Form::close()}}
    </fieldset>
</div>

<script>
     prepararNuevaNota();
</script>


@extends('dashboard')
@section('tab')        
<div id="hola" style="width: 200px">Hola soy una prueba.</div>

<div id="ohsnap"></div>
<!--
<div style="float: right">
<a href="#" class="btn btn-default" data-toggle="modal" data-target="#basicModal">Iniciar Sesion</a>
</div>

<div class="modal fade" id="basicModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">Iniciar Sesión</h4>
            </div>
            <div class="modal-body">
                <h3>Holi ;)</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save me!</button>
            </div>
        </div>
    </div>
</div>-->
<!--
<div id="frm-invitaciones" style="width: 200px;">
    {{Form::open(array('url' => 'api/invite', 'id' => 'invitar')) }}
    <input type="button" id="add" value="Agregar">
    <div class="correos">
        <input type="text" name="email[]" placeholder="ejemplo@correo.com"><br>
    </div>
    {{Form::submit('Invitar') }}
    {{Form::close() }}
</div>
<div id="adios" style="width: 200px">Hola soy otra prueba.</div>
<div id="calendar" style="width: 800px; height: 800px"></div>
-->
@stop

@section('scripts')
<script>
    $(function() {
        
        ohSnap('hoolaa', 'yellow');
        
        $("#add").click(function() {
            $(".correos").append('<div><input type="text" name="email[]" placeholder="ejemplo@correo.com">'
                    + '<input type="button" class="quitar" value="x"><br></div>');
        });
        $(".correos").on("click", ".quitar", function() {
            $(this).parent('div').remove();
        });

        $("#hola").qtip({
            content: {
                title: "hola",
                text: "desc"
            },
            show: {
                when: {
                    target: $("#adios")
                }
            },
            style: {
                name: "dark"
            }
        });
        // version funcionando con error en el resize
        $("#calendar").fullCalendar({
            header: {
                left: 'prev,today',
                center: 'title',
                right: 'today,next'
            },
            dayClick: function(date, jsEvent, view) {

            },
            eventClick: function(event, jsEvent, view) {

            },
            eventRender: function(event, element) {
                element.css('cursor', 'pointer');
                element.qtip({
                    content: {
                        title: event.title,
                        text: event.desc
                    },
                    position: {
                        corner: {
                            my: 'bottomMiddle',
                            at: 'topMiddle'
                        }
                    },
                    show: {
                        when: {
                            target: $('p')
                        }
                    }
                });
            }
        });
        //---------------------------------------------------------------


        var newEvent = new Object();
        newEvent.title = "Holi :v";
        newEvent.desc = "Hoy es un holi particularmente especial :v";
        newEvent.start = "Wed, 30 Oct 2014 13:00:00 EST";
        newEvent.allDay = false;

        var newEvent2 = new Object();
        newEvent2.title = "Wazzaa";
        newEvent2.desc = "wazaap event...";
        newEvent2.start = "2014-11-14";
        newEvent2.allDay = false;
        $('#calendar').fullCalendar('renderEvent', newEvent);
        $('#calendar').fullCalendar('renderEvent', newEvent2);
    });

    prepararInvitarUsuarios();


</script>
@stop 
<!--<form class="cmxform" id="commentForm" method="get" action="">
  <fieldset>
    <legend>Please provide your name, email address (won't be published) and a comment</legend>
    <p>
      <label for="cname">Name (required, at least 2 characters)</label>
      <input id="cname" name="name" minlength="2" type="text"/>
    </p>
    <p>
      <label for="cemail">E-Mail (required)</label>
      <input id="cemail" type="email" name="email"/>
    </p>
    <p>
      <label for="curl">URL (optional)</label>
      <input id="curl" type="url" name="url"/>
    </p>
    <p>
      <label for="ccomment">Your comment (required)</label>
      <textarea id="ccomment" name="comment" required></textarea>
    </p>
    <input class="" id="mobile_phone" name="mobile_phone">
    <p>
      <input class="submit" type="submit" value="Submit"/>
    </p>
  </fieldset>
</form>
<script>
$(function(){    
    
$( "#commentForm" ).validate({
  rules: {
    email: { required: true, email: true}
  },
  messages: {
      email: { required: "Es de awebo", email: "debe ser un email"}
  },
  submitHandler: function(form) {
      alert("Soy un submit activado o.o ~");
      form.submit();
  }
});

$("#commentForm").submit(function(e){
    
    e.preventDefault();
    e.unbind();
});

});
</script>
    </body>
</html>
-->
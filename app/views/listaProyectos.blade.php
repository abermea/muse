@extends('base')

@section('contenido')
<?php 
 $proyectos = json_decode($projects, true);
?>
        {{ $projects }}
        <h2>Mis Proyectos</h2>
        <table border="1">
            <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Borrar</th>
            </tr>
             @foreach($proyectos as $proyecto) 
            <tr>
                <td> {{ $proyecto['name']  }} </td>
                <td> {{ $proyecto['description']  }}</td>
                <td> 
                    <a class='lnk-project-delete' href="#">Eliminar</a>
                    <input type="hidden" name="idProject[]" value="{{ $proyecto['id']}}">
                </td>
            </tr>
            @endforeach
        </table>
@stop


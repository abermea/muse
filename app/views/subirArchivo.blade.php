
<div id="modal-create-file" title="Enviar Archivo">
    <fieldset>
        <legend>Subir archivo</legend>
        <input id="idProject" type="hidden" value="{{ $project }}"/>
        <!-- The fileinput-button span is used to style the file input field as button -->
        <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Selecciona tu archivo:</span>
            <!-- The file input field used as target for the file upload widget -->
            <input id="fileupload" type="file" name="files" multiple>
        </span>
        <br>
        <br>
        <!-- The global progress bar -->
        <div id="progress" class="progress">
            <div class="progress-bar progress-bar-success"></div>
        </div>
        <!-- The container for the uploaded files -->
        <div id="files" class="files"></div>
    </fieldset>
</div>


<script>
    $(function() {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = '<?php echo route('main') ?>/api/file-create';
        var dialog;
        
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            formData: {
                project: <?php echo $project ?>,
            },
            done: function(e, data) {
                $.each(data.result.files, function(index, file) {
                    $('<p/>').text(file.name).appendTo('#files');
                    var rowCount = $('#myTable tr').length;
                    $('#tableFiles tr:last').after('<td><input id="lnk-delete-file-id-'+rowCount+'" type="hidden" value="'+file.original_filename+'"/>'+file.original_filename+'</td><td><div id="lnk-delete-file-'+rowCount+'">x</div></td>');
                    prepareDeleteFile(rowCount);
                });
                dialog.dialog('close');
            },
            progressall: function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                        );
            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
                
        dialog = $("#modal-create-file").dialog({
            autoOpen: false,
            height: 200,
            width: 350,
            modal: true,
            buttons: {
            },
            close: function() {
                
            }
        });  
        
        $('#btn-new-file').button().on('click', function() {
            dialog.dialog('open');
        });
    });
</script>

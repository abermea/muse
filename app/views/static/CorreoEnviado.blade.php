@extends('static.template')

@section('content')
<div class="mainText">
    <div class="textPadding">
        <h1>Aviso de Privacidad</h1>
        <p>
        Sabemos lo importante que es la privacidad para ti y que necesitas la garantía de que tus datos no reciben un mal uso. No te preocupes, muse no comparte tu información personal con nadie más. Siéntete tranquilo, disfruta un café y organiza tus proyectos con la confianza de que tus datos son sólamente tuyos.
        <p>
        Ningún otro usuario puede acceder a tus datos personales.
        <p>
        
    </div>
</div>
<div class="mainText2">
    <div class="textPadding">
        <br>
        <img src="{{route('main')}}/img/sec2.jpg" class="imgMisc">
        <br>
    </div>
</div>
@stop
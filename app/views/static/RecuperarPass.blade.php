@extends('static.template')

@section('content')

<div class="correoEnviadoContainer">
    <h1>¿Has olvidado algo?</h1>
    <p>
    A todos nos pasa de vez en cuando, pero descuida, puedes reestablecer tu contraseña aquí mismo.
    <p>
        Te recomendamos hacer un poco de ejercicio, se dice que eso ayuda a la memoria.
        <p>
        Nueva contraseña<br>
        <input type="password">
        <p>
        <div class="enviar short">Aceptar</div>    
</div>

@stop


@extends('static.template')

@section('content')
<div class="mainText">
    <div class="textPadding">
        <h1>Copyright</h1>
        <p>
        
        Todos los derechos reservados. Ninguna parte de esta publicación puede ser reproducida, distribuida o transmitida en ninguna forma o bajo ningún medio, incluyendo fotocopia, grabación u otros medios electrónicos o mecánicos sin la previa autorización de Grupo Hydra, excepto en caso de breves citas dentro de material didáctico o de crítica y ciertos usos no comerciales permitidos por las leyes de copyright.
        <p>
        Para solicitudes de permisos, favor de contactar a Grupo Hydra.
        <p>
        Golondrina #113<br>
        Colonia Cuauhtémoc, San Nicolás de los Garza<br>
        Nuevo León, CP 66450<br>
        www.grupohydra.com.mx
        <p>
        
    </div>
</div>
<div class="mainText2">
    <div class="textPadding">
        <img src="{{route('main')}}/img/contact.jpg" class="imgMisc">
    </div>
</div>
@stop
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{route('main')}}/CSS/museMisc.css"/>
        <link rel="stylesheet" type="text/css" href="{{route('main')}}/CSS/museCore.css">
    </head>
    <body>
        
        <div class="upperMisc">
            <img src="{{route('main')}}/img/museLogo.png" id="imgLogo">
        </div>
        @yield('content')
        <div class="footerMisc"><a href="{{route('main')}}/copyright">Copyright</a> | <a href="{{route('main')}}/terminos">Términos y Condiciones</a> | <a href="{{route('main')}}/privacidad">Aviso de Privacidad</a> | <a href="{{route('main')}}/contacto">Contacto</a> | <a href="{{route('main')}}/faq">Preguntas Frecuentes</a></div>
    </body>
</html>
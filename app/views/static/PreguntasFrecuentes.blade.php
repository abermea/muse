@extends('static.template')

@section('content')
<div class="mainText">
    <div class="textPadding">
        <h1>Preguntas Frecuentes</h1>
        <p>
        <b>¿Qué costo tiene Muse?</b><br>
        ¡Muse es completamente gratis! Puedes organizar tus proyectos sin la preocupación de que vayan a expirar. Sin embargo, si prefieres funcionalidades adicionales, puedes revisar aquí nuestras tarifas de los servicios premium
        <p>
        <b>¿Qué tipo de clientes usan Muse?</b><br>
        Muse está diseñado para todo tipo de usuarios, desde un individuo que quiere organizar un proyecto personal hasta grandes empresas que necesitan un servicio robusto y confiable con la garantía de que en todo momento y lugar podrán revisar el progreso de sus proyectos.
        <p></p>
        <b>¿Qué uso se le da a mi información personal?</b><br>
        Todos los datos que ingresas en Muse son usados exclusivamente para funcionamiento interno de la aplicación, así que descuida, tu información está segura con nosotros.
        <p></p>
        <b>¿En qué plataformas puede ser usado Muse?</b><br>
        Como es una aplicación web, Muse puede ser usado en cualquier sistema operativo (Windows, Linux, Mac, Android, iOS), ya que no necesitas instalar nada en tu computadora para que funcione adecuadamente.
        <p>
        
    </div>
</div>
<div class="mainText2">
    <div class="textPadding">
        <img src="{{route('main')}}/img/faq.jpg" class="imgMisc">
    </div>
</div>
@stop
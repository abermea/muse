<div class="modal fade" id="modalCrearGrupo" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">Nuevo grupo</h4>
            </div>
            <div class="modal-body">
                <div id='frm-newGroup'>
                        {{Form::open(array('url' => 'api/group-create', 'id' => 'nuevoGrupo') )}}
                        <input type="text" name="nombre" id="nombre" placeholder="Nombre del grupo">
                        <textarea type="text" name="desc" id="desc"></textarea>
                        {{Form::close()}}
                        <div id="alertGrupo" class="alert alert-warning" style="display: none;">
                            <a href="#" class="close">&times;</a>
                            <span id="mensaje">...</span>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="btn-grupo" class="enviar">Crear</div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
       $("#btn-grupo").click(function(){
           $("#alertGrupo").removeClass('alert-danger').addClass('alert-warning');
            $("#mensaje").text("Creando...");
            $("#alertGrupo").show();
            $("#nuevoGrupo").submit();
       });

       prepararNuevoGrupo();
    });
</script>
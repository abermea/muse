<div class="modal fade" id="modalCrearEvento" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">Nuevo evento</h4>
            </div>
            <div class="modal-body">
                <div id='frm-newEvent'>
                        {{Form::open(array('url' => 'api/appointment-create', 'id' => 'nuevoEvento') )}}
                        <input type="hidden" name="project" value="{{ $project }}"/>
                        <input type="text" name="nombre" id="nombre" placeholder="Nombre del evento"><br><br>
                        <textarea name="desc" id="desc" placeholder="Descripción" ></textarea><br><br>
                        <input type="text" name="fechaInicio" id="fechaInicio" placeholder="Fecha de Inicio"><br>
                        <input type="text" name="fechaFin" id="fechaFin" placeholder="Fecha de Fin"><br><br>
                        <input type="text" name="horaInicio" id="horaInicio" placeholder="Hora de Inicio"><br>
                        <input type="text" name="horaFin" id="horaFin" placeholder="Hora de Fin"><br><br>
                        <input type="text" name="lugar" id="lugar" placeholder="Lugar"><br><br>
                        {{Form::close()}}
                        <div id="alertEvento" class="alert alert-warning" style="display: none;">
                            <a href="#" class="close">&times;</a>
                            <span id="mensaje2">...</span>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="btn-evento" class="enviar">Crear</div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){        
        $("#btn-evento").click(function(){
            $("#alertEvento").show();
           $("#nuevoEvento").submit() ;
        });
            
    $(".close").click(function(){
        $("#alertEvento").hide();
    });
    
     $("#fechaInicio").datepicker($.datepicker.regional[ 'es' ]);
     $("#fechaFin").datepicker($.datepicker.regional[ 'es' ]);
     $("#horaInicio").timepicker({'timeFormat':'h:i A'});
     $("#horaFin").timepicker({'timeFormat':'h:i A'});
     prepararNuevoEvento();
    });
</script>


<a href="#" class="btn btn-default" data-toggle="modal" data-target="#basicModal">Iniciar Sesión</a>
    
<div class="modal fade" id="basicModal" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">Iniciar Sesión</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="login-username" class="userInput" placeholder="usuario" name="alias">
                <br><br>
                <input type="password" id="login-password" class="passInput" placeholder="password" name="alias">
                <br><br>
                <input type="checkbox" id="login-remember-me"> <label for="login-remember-me"> Recordarme</label>
                <div id="alertSesion" class="alert alert-warning" style="display: none;">
                    <a href="#" class="close">&times;</a>
                    <span id="mensaje">Verificando...</span>
                </div>
            </div>
            <div class="modal-footer">
                <div id="btn-login" class="enviar">Iniciar Sesión</div>
                <br>
                <a href="forgot-password">Olvidé mi contraseña</a>
            </div>
        </div>
    </div>
</div>


<script>
    $(function(){
       $("#frm-login").hide();

    $('#lnk-login-modal').button().on('click', function(event) {
        event.preventDefault();
        if (!$("#frm-login").is(':visible')) {
            $("#frm-login").show();
        } else {
            $("#frm-login").hide();
        }
    });

    $(".close").click(function(){
        $("#alertSesion").hide();
    });
    
    $('#btn-login').button().on('click', function(event) {
        var postData = {
        'alias': $('#login-username').val(),
        'password': $('#login-password').val(),
        'remember-me': checkboxChecked('#login-remember-me')
    };
        //$("#verificando").html('<br>Verificando...').show();
        $("#mensaje").text("Verificando...");
        $("#alertSesion").show();
        attemptLogin(postData);
        event.preventDefault(); //STOP default action
    }); 
    });
</script>

<?php $user = API::get('api/user', array('alias' => Auth::user()->id)) ?>
<div class="modal fade" id="modalPerfilUsuario" >
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">{{ $user['alias'] }}</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('url' => '/api/user-update', 'id' => 'frm-edit-user', 'method' => 'POST')) }}
                <input type="text" name="first-name" placeholder="Nombres" value="{{ $user['first_name'] }}">
                <input type="text" name="last-name" placeholder="Apellidos" value="{{ $user['last_name'] }}">
                <input type="text" name="alias" placeholder="Alias" value="{{ $user['alias'] }}">
                <input type="text" name="email" placeholder="Correo electrónico" value="{{ $user['email'] }}">
                <input type="password" name="new-password" placeholder="Nueva contraseña">
                <input type="text" name="message" placeholder="Breve descripción" value="{{ $user['message'] }}">
                <input type="password" name="old-password" placeholder="Confirma los cambios con tu contraseña">
                {{ Form::submit('Listo!') }}
                {{ Form::close()}}
                <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
</div>

<script>
$(function() {
    $('#userPic').click(function() {
        $('#modalPerfilUsuario').modal('show');
    });

    prepararFormEditarUsuario();
});
    
</script>
<?php $users = API::get('api/users-by-project', array('project' => $project)) ?>
<div class="modal fade" id="modalTarea" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva tarea</h4>
            </div>
            <div class="modal-body">
                <!-- Form tareas -->
                <div id='frm-new-task' title="Nueva Tarea">
                {{Form::open(array('url' => 'api/activity-create', 'id' => 'nuevaTarea') )}}
                <input type="hidden" name="project" value="{{ $project }}"/>
                <input type="text" name="nombre" placeholder="Nombre de la Tarea"><br><br>
                <textarea name="desc" placeholder="Descripción" ></textarea><br><br>
                <div id="tareaFechas">
                <input type="text" name="fechaInicio" id='fechaInicioAct' placeholder="Fecha de Inicio">
                <input type="text" name="fechaFin" id='fechaFinAct' placeholder="Fecha de Fin">
                <br><br>
                <input type="text" name="horaInicio" id='horaInicioAct' placeholder="Hora de Inicio">
                <input type="text" name="horaFin" id='horaFinAct' placeholder="Hora de Fin">
                </div><br>
                <div id="tareaOtros">
                <select name="priority" id="priority" >
                    <option value="-1">Seleccione un Nivel de Prioridad:</option>
                    <option value="3">Baja</option>
                    <option value="2">Media</option>
                    <option value="1">Alta</option>
                </select>
                <select name="responsible" id="responsible" >
                    <option value="-1">Seleccione un Usuario Responsable:</option>
                    @foreach($users as $user)
                    <option value="{{$user['id']}}">{{$user['first-name'].' '.$user['last-name']}}</option>
                    @endforeach
                </select>
                </div><br>
                {{Form::close()}}
                </div>
                <!-- Form tareas -->
                <span id="verificando" style="display: none"><br>Verificando...</span>
            </div>
            <div class="modal-footer">
                <div id="btn-tarea" class="enviar">Crear</div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
       $("#btn-tarea").click(function(){
           $("#nuevaTarea").submit();
       });
       
       $("#fechaInicioAct").datepicker($.datepicker.regional[ 'es' ]);
        $("#fechaFinAct").datepicker($.datepicker.regional[ 'es' ]);
        $("#horaInicioAct").timepicker({'timeFormat': 'h:i A'});
        $("#horaFinAct").timepicker({'timeFormat': 'h:i A'});
    });
</script>
<div class="modal fade" id="modalVistaNota" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel"> </h4>
            </div>
            <div class="modal-body">
                <div id='frm-newNotes'>
                        {{Form::open(array('url' => 'api/note-create', 'id' => 'nuevaNota') )}}
                        <input id="note-title" type="text" name="name" value="" placeholder="Nombre"/><br><br>
                        <textarea style="width: 100%; height:40%;" name="contenido" id="contenido" placeholder="Escribe tu nota" wrap="hard"></textarea><br/>
                        {{Form::hidden('idUser', Auth::user()->id, array('id' => 'idUser')) }}
                        {{Form::hidden('idProject', $project, array('id' => 'idProject')) }}
                        {{Form::close()}}
                        <div id="alertNota" class="alert alert-warning" style="display: none;">
                            <a href="#" class="close">&times;</a>
                            <span id="mensaje">Cargando nota...</span>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="btn-crearNota" class="enviar">Crear</div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#btn-crearNota").click(function(){
        $("#alertNota").removeClass('alert-danger').addClass('alert-warning');
        $("#mensaje").text('Creando...');
        $("#alertNota").show();
       $("#nuevaNota").submit(); 
    });
    
    $(".close").click(function(){
        $("#alertNota").hide();
    });
    prepararNuevaNota();
</script>
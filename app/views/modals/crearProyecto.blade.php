<div class="modal fade" id="modalCrearProyecto" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></button>
                <h4 class="modal-title" id="myModalLabel">Nuevo proyecto</h4>
            </div>
            <div class="modal-body">
                <div id='frm-newProyect' title='Nuevo Proyecto'>
                        {{Form::open(array('url' => 'api/project-create', 'id' => 'nuevoProyecto') )}}
                        <input type="text" name="titulo" id="titulo" placeholder="Título"><br><br>
                        <input type="text" name="desc" id="desc" placeholder="Descripción"><br><br>
                        <input type="text" name="fechaInicio" id="fechaInicio" placeholder="Fecha de Inicio"><br><br>
                        <input type="text" name="fechaFin" id="fechaFin" placeholder="Fecha de Fin"><br>
                        {{Form::close()}}
                        <div id="alertProyecto" class="alert alert-warning" style="display: none;">
                            <a href="#" class="close">&times;</a>
                            <span id="mensaje">...</span>
                        </div>
                </div>          
            </div>
            <div class="modal-footer">
                <div id="btn-crearProyecto" class="enviar">Crear</div>
            </div>
        </div>
    </div>
</div>


<script>
    $(function() {
        
        $("#btn-new-project").click(function(){
           $("#modalCrearProyecto").modal('show'); 
        });
        
        $("#fechaInicio").datepicker($.datepicker.regional[ 'es' ]);
        $("#fechaFin").datepicker($.datepicker.regional[ 'es' ]);
        
        $("#btn-crearProyecto").click(function(){
            $("#alertProyecto").removeClass('alert-danger').addClass('alert-warning');
            $("#mensaje").text("Creando...");
            $("#alertProyecto").show();
            $("#nuevoProyecto").submit();
        });
        
        
    $(".close").click(function(){
        $("#alertProyecto").hide();
    });
    
        prepararNuevoProyecto();
    });
</script>
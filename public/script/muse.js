function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam)
        {
            return sParameterName[1];
        }
    }
}

function prepararControlesUsuario() {
    $('#lnk-logout').click(function(e) {
        var actionURL = $(this).attr("href");
        $.ajax({
            url: actionURL,
            type: 'POST',
            success: function(data, textStatus, jqXHR)
            {
                alert("yei :D");
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert("noou D:");
            }
        });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

//callback handler for form submit
function prepararFormDeRegistro() {
    $("#iniciando").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        alert("yei :D");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("noou D:");
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararFormDeRegistroCompleta() {
    $("#registro").submit(function(e)
    {
        $("#verificando").html('<br>Verificando...').show();
        var postData = {
            'alias': $(this).find('input[name="alias"]').val(),
            'password': $(this).find('input[name="password"]').val(),
            'first-name': $(this).find('input[name="first-name"]').val(),
            'last-name': $(this).find('input[name="last-name"]').val(),
            'email': GetURLParameter('email'),
            'desc': $(this).find('textarea[name="message"]').val()
        };
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        $("#verificando").html('<br>Listo');
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $("#verificando").html('<br>' + textStatus + ': ' + errorThrown);
                    }
                });
        e.preventDefault(); //STOP default action
        //e.unbind(); //unbind. to stop multiple form submit.
    });
}

function validarEmailRegistrado() {
    var serviceURL = 'api/check-mail-validation-code';
    var postData = {
        'email': GetURLParameter('email'),
        'validation-code': GetURLParameter('validation-code')
    };
    $.ajax(
            {
                url: serviceURL,
                type: "POST",
                data: postData,
                success: function(data, textStatus, jqXHR)
                {
                    $("#frm-registro").show();
                    $("#validando").hide();
                    prepararFormDeRegistroCompleta();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $("#verificando").html('<br>' + textStatus + ': ' + errorThrown);
                }
            });
}

function prepararFormInicioSesion() {
    $("#login").submit(function(e)
    {
        //var postData = $(this).serializeArray();
        var postData = {
            'alias': $(this).find('input[name="alias"]').val(),
            'password': $(this).find('input[name="password"]').val(),
            'remember-me': checkboxChecked('#remember-me')
        };
        var formURL = "api/authenticate";
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        alert("yei :D");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("noou D:");
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararFormEditarUsuario() {
    $("#frm-edit-user").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        alert("yei :D");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("noou D:");
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararFormRecuperarContrasena() {
    $("#recuperarCon").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        alert("yei :D");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("noou D:");
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararFormRecuperarContrasena2() {
    $("#recuperarCon2").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        alert("yei :D");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("noou D:");
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function checkboxChecked(id) {
    if ($(id).is(":checked"))
        return true;
    return false;
}


function prepararControlesUsuario() {
    $('#lnk-user-delete').click(function(e) {
        var actionURL = $(this).attr("href");
        $.ajax({
            url: actionURL,
            type: 'POST',
            success: function(data, textStatus, jqXHR)
            {
                alert("yei :D");
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert("noou D:");
            }
        });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararNuevoProyecto() {
    $("#nuevoProyecto").submit(function(e)
    {
        var postData = {
            'name': $(this).find('input[name="titulo"]').val(),
            'description': $(this).find('input[name="desc"]').val(),
            'start-date': $(this).find('input[name="fechaInicio"]').val(),
            'end-date': $(this).find('input[name="fechaFin"]').val()
        };
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        $("#mensaje").text("Proyecto creado exitosamente");
                        $("#scroller a:last").after('<a href="proyecto/'+data.id+'"><div class="projectThumb"><div class="textProjectThumb">'+data.name+'</div></div></a>');
                        $("#modalCrearProyecto").modal('hide');
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $("#alertProyecto").removeClass('alert-warning').addClass('alert-danger');
                        $("#mensaje").text("Error: " + errorThrown);
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararInvitarUsuarios() {
    $("#invitar").submit(function(e)
    {
        $("#mensaje").text('Enviando...');
        $("#alertInvitar").show();
        var correos = $("input[name='email\\[\\]']").map(function() {
            return $(this).val();
        }).get();
        var postData = {
            'emails': correos,
            'project' : $('input[name="project"]').val()
        };
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        $("#mensaje").text('Se han invitado a estos correos: ' + correos);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $("#alertInvitar").removeClass('alert-warning').addClass('alert-danger');
                        $("#mensaje").text('Error');
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function scriptInvitaciones() {
    var serviceURL = 'api/';
    var postData = {
        'email': GetURLParameter('email'),
        'validation-code': GetURLParameter('validation-code')
    };
    $.ajax(
            {
                url: serviceURL,
                type: "POST",
                data: postData,
                success: function(data, textStatus, jqXHR)
                {
                    alert("yei :D");
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    alert("nou D:");
                }
            });
}

function prepararNuevaNota() {
    $("#nuevaNota").submit(function(e)
    {
        var numberOfRows = $("#tableFiles tr").length -1;
        var postData = {
            'name' : $(this).find('input[name="name"]').val(),
            'project': $(this).find('input[name="idProject"]').val(),
            'content': $(this).find('textarea[name="contenido"]').val()
        };
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        $("#mensaje").text('Nota creada exitosamente.');
                        $("#modalCrearNota").modal('hide');
                        $("#tableFiles tr:last").last().after('<tr id="row-note-'+ numberOfRows +'"><td><input id="lnk-delete-note-id-'+ numberOfRows +'" type="hidden" value="'+ data.note.id +'"/>'+ data.note.name +'</td><td><div id="lnk-delete-note-'+ numberOfRows +'" class="close"><span class="glyphicon glyphicon-remove"></span></div></td></tr>');
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $("#alertNota").removeClass('alert-warning').addClass('alert-danger');
                        $("#mensaje").text('Error: ' + errorThrown);
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararEliminarProyecto() {
    $('.lnk-project-delete').click(function(e) {
        var actionURL = 'http://localhost/muse/public/api/project-delete';
        $.ajax({
            url: actionURL,
            type: 'POST',
            success: function(data, textStatus, jqXHR)
            {
                
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                
            }
        });
        e.preventDefault(); //STOP default action
    });
}

function prepararSubirArchivo() {
    $("#subirArchivo").submit(function(e)
    {
        var postData = {
            'file': $(this).find('input[name="file"]').val(),
            'project': '4',
        };
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    contentType: false,
                    success: function(data, textStatus, jqXHR)
                    {
                        alert("yei :D");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("noou D:");
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararNuevoGrupo() {
    $("#nuevoGrupo").submit(function(e)
    {
        var postData = {
            'name': $(this).find('input[name="nombre"]').val(),
            'description': $(this).find('textarea[name="desc"]').val()
        };
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        $("#mensaje").text("Grupo creado exitosamente");
                        $("#scroller a:last").after('<a href="grupo/'+data.id+'"><div class="projectThumb"><div class="textProjectThumb">'+data.name+'</div></div></a>');
                        $("#modalCrearGrupo").modal('hide');
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $("#alertGrupo").removeClass('alert-warning').addClass('alert-danger');
                        $("#mensaje").text("Error: " + errorThrown);
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararNuevoEvento() {
    $("#nuevoEvento").submit(function(e)
    {
        var postData = {
            'project': $(this).find('input[name="project"]').val(),
            'name': $(this).find('input[name="nombre"]').val(),
            'description': $(this).find('input[name="desc"]').val(),
            'start_date': $(this).find('input[name="fechaInicio"]').val(),
            'end_date': $(this).find('input[name="fechaFin"]').val(),
            'start_time': $(this).find('input[name="horaInicio"]').val(),
            'end_time': $(this).find('input[name="horaFin"]').val(),
            'location': $(this).find('input[name="lugar"]').val()
        };
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        alert("yei :D");
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("noou D:");
                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function prepararNuevaTarea() {
    $("#nuevaTarea").submit(function(e) {
        var postData = {
            'project': $(this).find('input[name="project"]').val(),
            'name': $(this).find('input[name="nombre"]').val(),
            'description': $(this).find('textarea[name="desc"]').val(),
            'priority': $(this).find('select[name="priority"]').val(),
            'responsible': $(this).find('select[name="responsible"]').val(),
            'start_date': $(this).find('input[name="fechaInicio"]').val(),
            'end_date': $(this).find('input[name="fechaFin"]').val(),
            'start_time': $(this).find('input[name="horaInicio"]').val(),
            'end_time': $(this).find('input[name="horaFin"]').val()
        };
        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        $("#frm-new-task").dialog('close');
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {

                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
}

function renderEvent(calEvent) {
    var newEvent = new Object();
    newEvent.title = calEvent.name;
    newEvent.desc = calEvent.description;
    newEvent.start = calEvent.begin_date;
    newEvent.end = calEvent.end_date;
    newEvent.allDay = false;
    return newEvent;
}